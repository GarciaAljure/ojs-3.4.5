<?php /* Smarty version 2.6.26, created on 2015-07-29 22:31:47
         compiled from file:C:%5Cwamp%5Cwww%5Cojs/plugins/importexport/pluginPersonalizable/vista/templates.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'translate', 'file:C:\\wamp\\www\\ojs/plugins/importexport/pluginPersonalizable/vista/templates.tpl', 19, false),array('function', 'plugin_url', 'file:C:\\wamp\\www\\ojs/plugins/importexport/pluginPersonalizable/vista/templates.tpl', 27, false),)), $this); ?>
<?php echo ''; ?><?php $this->assign('pageTitle', "plugins.importexport.PluginPersonalizable.titleTemplate"); ?><?php echo ''; ?><?php $this->assign('pageCrumbTitle', "plugins.importexport.PluginPersonalizable.titleTemplate"); ?><?php echo ''; ?><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "common/header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?><?php echo ''; ?>


<br/>

<a href="#" id="createTemplate" class="action"><?php echo $this->_plugins['function']['translate'][0][0]->smartyTranslate(array('key' => "plugins.importexport.PluginPersonalizable.createTemplate"), $this);?>
</a>
<a href="#" id="importTemplate" class="action"><?php echo $this->_plugins['function']['translate'][0][0]->smartyTranslate(array('key' => "plugins.importexport.PluginPersonalizable.import"), $this);?>
</a>

<div id="xmlImport">

    <h3><?php echo $this->_plugins['function']['translate'][0][0]->smartyTranslate(array('key' => "plugins.importexport.PluginPersonalizable.import"), $this);?>
</h3>
    <p><?php echo $this->_plugins['function']['translate'][0][0]->smartyTranslate(array('key' => "plugins.importexport.PluginPersonalizable.import.description"), $this);?>
</p>

    <form action="<?php echo $this->_plugins['function']['plugin_url'][0][0]->smartyPluginUrl(array('path' => 'import'), $this);?>
" method="post" enctype="multipart/form-data">
        <input type="file" class="uploadField" name="importFile" id="import" /> <input name="import" type="submit" class="button defaultButton" value="<?php echo $this->_plugins['function']['translate'][0][0]->smartyTranslate(array('key' => "common.import"), $this);?>
" />
        <input type="hidden" name="temporaryFileId" id="temporaryFileId" value="" />

    </form>
</div>

<br>





<div  id="xmlTree">
    <h3><?php echo $this->_plugins['function']['translate'][0][0]->smartyTranslate(array('key' => "plugins.importexport.PluginPersonalizable.createTree"), $this);?>
</h3>
    <p><?php echo $this->_plugins['function']['translate'][0][0]->smartyTranslate(array('key' => "plugins.importexport.PluginPersonalizable.createTree.description"), $this);?>
</p>

    <form action="<?php echo $this->_plugins['function']['plugin_url'][0][0]->smartyPluginUrl(array('path' => 'CreateTreeTemplate'), $this);?>
" method="post" enctype="multipart/form-data" id="formTree">
        <label><?php echo $this->_plugins['function']['translate'][0][0]->smartyTranslate(array('key' => "plugins.importexport.PluginPersonalizable.name"), $this);?>
</label>
        <input name="name" id="name" type="text" class="textField" value="" />
        <input name="html" id="html" type="hidden" class="textField" value="" />
        <input name="plugin_url" id="plugin_url" type="hidden" class="textField" value="<?php echo $this->_plugins['function']['plugin_url'][0][0]->smartyPluginUrl(array(), $this);?>
" />
        <input name="import"  onclick="copyHTMLtoInput()" type="button" class="button defaultButton"  value="<?php echo $this->_plugins['function']['translate'][0][0]->smartyTranslate(array('key' => "common.confirm"), $this);?>
" />
    </form>

    <br>
    <br>

    <div class="easy-tree" >

        <ul id="treeHtml">
            <li id="startUl">Inicio</li>

        </ul>
    </div>
</div>



<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "common/footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<script >
    <?php echo '


        $(document).ready(function () {

            $("#xmlTree").hide();
            $("#xmlImport").hide();


            $("#createTemplate").click(function () {
                $("#xmlImport").hide();
                $("#xmlTree").show();

            });
            $("#importTemplate").click(function () {
                $("#xmlTree").hide();
                $("#xmlImport").show();
            });


        });


        $(window).load(function () {
            $(\'.easy-tree\').EasyTree({
                addable: true,
                editable: true,
                deletable: true
            });

        });

        function copyHTMLtoInput() {
            $("#html").val($("#treeHtml").html());
            $("#formTree").submit();
        }



    '; ?>

</script>
