<?php /* Smarty version 2.6.26, created on 2015-08-17 01:40:59
         compiled from file:C:%5Cwamp%5Cwww%5Cojs/plugins/importexport/pluginPersonalizable/vista/markup.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'translate', 'file:C:\\wamp\\www\\ojs/plugins/importexport/pluginPersonalizable/vista/markup.tpl', 20, false),array('function', 'plugin_url', 'file:C:\\wamp\\www\\ojs/plugins/importexport/pluginPersonalizable/vista/markup.tpl', 22, false),array('modifier', 'escape', 'file:C:\\wamp\\www\\ojs/plugins/importexport/pluginPersonalizable/vista/markup.tpl', 27, false),)), $this); ?>

<?php echo ''; ?><?php $this->assign('pageTitle', "plugins.importexport.PluginPersonalizable.export.markup"); ?><?php echo ''; ?><?php $this->assign('pageCrumbTitle', "plugins.importexport.PluginPersonalizable.export.markup"); ?><?php echo ''; ?><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "common/header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?><?php echo ''; ?>


<div id="issues">
    <h3 style="display: inline; width: 50%; " ><?php echo $this->_plugins['function']['translate'][0][0]->smartyTranslate(array('key' => "plugins.importexport.PluginPersonalizable.textEditor"), $this);?>
</h3>
    <?php if ($this->_tpl_vars['falseId'] == 0): ?>
        <form style="   float: right;  margin-top: 5px; cursor: pointer;" action="<?php echo $this->_plugins['function']['plugin_url'][0][0]->smartyPluginUrl(array('path' => 'selectedArticle'), $this);?>
" method="post" id="articleList" >
            <a  onclick="submitAtag('articleList')" class="action"><?php echo $this->_plugins['function']['translate'][0][0]->smartyTranslate(array('key' => "plugins.importexport.PluginPersonalizable.next"), $this);?>
</a>  
            <input name="articleId" id="articleId" type="hidden" value="<?php echo $this->_tpl_vars['articleId']; ?>
"  />
            <input name="issueId" id="issueId" type="hidden" value="<?php echo $this->_tpl_vars['issueId']; ?>
"  />
            <input name="templateId" id="templateId" type="hidden" value="<?php echo $this->_tpl_vars['template']->getId(); ?>
"  />
            <input type="hidden" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['htmlNoMarkup'])) ? $this->_run_mod_handler('escape', true, $_tmp) : $this->_plugins['modifier']['escape'][0][0]->smartyEscape($_tmp)); ?>
" id="selectedArticleHtml" name="selectedArticleHtml">
            <input type="hidden"  id="articleListHtml" name="articleListHtml">
        </form>
    <?php else: ?>
        <form style="   float: right;  margin-top: 5px; cursor: pointer;" action="<?php echo $this->_plugins['function']['plugin_url'][0][0]->smartyPluginUrl(array('path' => 'exportIssueMarkup'), $this);?>
" method="post" id="exportIssue" >
            <a  onclick="submitExport('exportIssue')" class="action"><?php echo $this->_plugins['function']['translate'][0][0]->smartyTranslate(array('key' => "plugins.importexport.PluginPersonalizable.exportIssue"), $this);?>
</a>  
            <input name="articleId" id="articleId" type="hidden" value="<?php echo $this->_tpl_vars['articleId']; ?>
"  />
            <input name="issueId" id="issueId" type="hidden" value="<?php echo $this->_tpl_vars['issueId']; ?>
"  />
            <input name="templateId" id="templateId" type="hidden" value="<?php echo $this->_tpl_vars['template']->getId(); ?>
"  />
            <input type="hidden" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['htmlNoMarkup'])) ? $this->_run_mod_handler('escape', true, $_tmp) : $this->_plugins['modifier']['escape'][0][0]->smartyEscape($_tmp)); ?>
" id="exportIssueMarkupHtml" name="exportIssueMarkupHtml">
            <input type="hidden"  id="exportIssueHtml" name="exportIssueHtml">
            <input type="hidden"  id="exportScielo" name="exportScielo">
        </form>

        <form style="   float: right;  margin-top: 5px; cursor: pointer; margin-right: 10px;" action="<?php echo $this->_plugins['function']['plugin_url'][0][0]->smartyPluginUrl(array('path' => 'saveAndExitMarkup'), $this);?>
" method="post" id="saveAndExit" >
            <a  onclick="submitAtag('saveAndExit')" class="action"><?php echo $this->_plugins['function']['translate'][0][0]->smartyTranslate(array('key' => "plugins.importexport.PluginPersonalizable.save"), $this);?>
</a>  
            <input name="articleId" id="articleId" type="hidden" value="<?php echo $this->_tpl_vars['articleId']; ?>
"  />
            <input name="issueId" id="issueId" type="hidden" value="<?php echo $this->_tpl_vars['issueId']; ?>
"  />
            <input name="templateId" id="templateId" type="hidden" value="<?php echo $this->_tpl_vars['template']->getId(); ?>
"  />
            <input type="hidden" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['htmlNoMarkup'])) ? $this->_run_mod_handler('escape', true, $_tmp) : $this->_plugins['modifier']['escape'][0][0]->smartyEscape($_tmp)); ?>
" id="saveAndExitMarkupHtml" name="saveAndExitMarkupHtml">
            <input type="hidden"  id="saveAndExitHtml"  name="saveAndExitHtml">
        </form>

    <?php endif; ?>

    <label style="display: block; margin-top: 5px; "><?php echo $this->_plugins['function']['translate'][0][0]->smartyTranslate(array('key' => "plugins.importexport.PluginPersonalizable.editorText"), $this);?>
</label>
    <input type="hidden" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['htmlMarkup'])) ? $this->_run_mod_handler('escape', true, $_tmp) : $this->_plugins['modifier']['escape'][0][0]->smartyEscape($_tmp)); ?>
" id="docxText">
    
    <p><?php echo $this->_plugins['function']['translate'][0][0]->smartyTranslate(array('key' => "plugins.importexport.PluginPersonalizable.copySelectedText"), $this);?>
</p>

    <textarea name="editor1" id="editor1" style="width: 100%; margin-top: 10px;"></textarea>
</div>
<h3><?php echo $this->_plugins['function']['translate'][0][0]->smartyTranslate(array('key' => "plugins.importexport.PluginPersonalizable.Tree"), $this);?>
</h3>
<p><?php echo $this->_plugins['function']['translate'][0][0]->smartyTranslate(array('key' => "plugins.importexport.PluginPersonalizable.asignTree"), $this);?>
</p>
<div class="easy-tree" id="xmlTree">
    <ul id="treeHtml">
        <?php echo $this->_tpl_vars['template']->getText(); ?>


    </ul>
</div>

<div id="menu" class="divMenu">
    <ul class="ulMenu">
        <li id="asign" class="liMenu"><?php echo $this->_plugins['function']['translate'][0][0]->smartyTranslate(array('key' => "plugins.importexport.PluginPersonalizable.AsignTag"), $this);?>
</li>
        <li id="author" class="liMenu"><?php echo $this->_plugins['function']['translate'][0][0]->smartyTranslate(array('key' => "plugins.importexport.PluginPersonalizable.markupAuthor"), $this);?>
</li>
        <li id="cite" class="liMenu"><?php echo $this->_plugins['function']['translate'][0][0]->smartyTranslate(array('key' => "plugins.importexport.PluginPersonalizable.markupCite"), $this);?>
</li>
    </ul>
</div>


<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "common/footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<script >
    <?php echo '
        var elementHtml = null;
        var selectedWord = null;
        var list_html = null;

        CKEDITOR.replace(\'editor1\', {
            extraPlugins: \'resize,image,table\',
            removePlugins: \'elementspath\'
        });
        //var nuevo = jQuery.noConflict();
        $(window).load(function () {
            $(\'.easy-tree\').EasyTree({
                addable: false,
                editable: false,
                deletable: false
            });
            $("#treeHtml").find("a").each(function (index, element) {
                //console.log(\'El elemento con el índice \' + index + \' contiene \' + $(element).text());
                $(element).bind("contextmenu", function (e) {
                    elementHtml = element;
                    $("#menu").css({\'display\': \'block\', \'left\': e.pageX, \'top\': e.pageY});
                    return false;
                });
            });
            CKEDITOR.instances.editor1.setData($("#docxText").val());
            //console.log($(".divMenu"));
            //console.log(CKEDITOR.document.getElementsByTag("table").$);

            /*$(CKEDITOR.instances.editor1).find("p").each(function (index, element) {
             //console.log(\'El elemento con el índice \' + index + \' contiene \' + $(element).text());
             console.log($(element));
             });
             
             */

        });
        $(window).ready(function () {


            CKEDITOR.instances.editor1.on(\'contentDom\', function () {

                $(CKEDITOR.instances.editor1.document.getElementsByTag("table").$).each(function (index, element) {
                    if ($(element).html() !== "") {

                        $(element).tableExport({type: \'png\', escape: \'false\', index: index});
                        //$(element).replaceWith("<img src=\'/../ojs/plugins/importexport/pluginPersonalizable/image/" + index + "-" + $("#issueId").val() + "-" + $("#articleId").val() + ".png\'> ");


                    }
                    //console.log(\'El elemento con el índice \' + index + \' contiene \' + $(element).html());
                    // console.log($(element));
                });
            });
            var array = $("#treeHtml").find("a");
            $("#menu").hide();
            /* mostramos el menú si hacemos click derecho
             con el ratón */
            $(document).bind("contextmenu", function (e) {

                return false;
            });
            //cuando hagamos click, el menú desaparecerá
            $(document).click(function (e) {
                if (e.button == 0) {
                    $("#menu").css("display", "none");
                }
            });
            //si pulsamos escape, el menú desaparecerá
            $(document).keydown(function (e) {
                if (e.keyCode == 27) {
                    $("#menu").css("display", "none");
                }
            });
            //controlamos los botones del menú
            $("#menu").click(function (e) {

                // El switch utiliza los IDs de los <li> del menú
                switch (e.target.id) {
                    case "asign":
                        var editorC = CKEDITOR.instances.editor1;
                        if (editorC.getSelectedHtml() === null || editorC.getSelectedHtml().getHtml().toString() === "") {
                            alert("Debe seleccionar el texto para realizar esta función");
                            break;
                        }
                        var selectedContent = editorC.getSelectedHtml().getHtml();
                        /*var mySelection = editorC.getSelection();
                         
                         var selectedContent= "";
                         if (mySelection.getType() === CKEDITOR.SELECTION_ELEMENT) {
                         
                         selectedContent = getSelectionHtml(editorC);
                         } else if (mySelection.getType() === CKEDITOR.SELECTION_TEXT) {
                         
                         if (CKEDITOR.env.ie) {
                         mySelection.unlock(true);
                         selectedContent = mySelection.getNative().createRange().text;
                         } else {
                         selectedContent = mySelection.getNative();
                         
                         }
                         }
                         */

                        /*   var selectedText = "";
                         if (CKEDITOR.env.ie) {
                         mySelection.unlock(true);
                         selectedText = mySelection.getNative().createRange().text;
                         } else {
                         selectedText = mySelection.getNative();
                         }
                         */


                        //console.log("selected html " + CKEDITOR.instances.editor1.getSelectedHtml().getHtml());


                        //if (mySelection.getType() === CKEDITOR.SELECTION_ELEMENT) {
                        editorC.insertHtml(escapeHtml("<" + $.trim($(elementHtml).text()) + ">") + selectedContent + escapeHtml("</" + $.trim($(elementHtml).text()) + ">"));

                        elementHtml = null;
                        selectedWord = null;
                        list_html = null;

                        // } else if (mySelection.getType() === CKEDITOR.SELECTION_TEXT) {
                        //     editorC.insertText("<" + $.trim($(elementHtml).text()) + ">" + selectedContent + "</" + $.trim($(elementHtml).text()) + ">");
                        //}

                        //var edata = editorC.getData();

                        //var plainText = editorC.document.getBody().getText();

                        //var replaced_text = edata.replace(escapeHtml(selectedText), escapeHtml("<" + $.trim($(elementHtml).text()) + ">") + escapeHtml(selectedText) + escapeHtml("</" + $.trim($(elementHtml).text()) + ">"));

                        //editorC.setData(replaced_text);

                        //CKEDITOR.instances.editor1.setData("<articulo>This is the editor data.</articulo>");
                        break;
                    case "author":
                        var editorC = CKEDITOR.instances.editor1;
                        if (editorC.getSelectedHtml() === null || editorC.getSelectedHtml().getHtml().toString() === "") {
                            alert("Debe seleccionar el texto para realizar esta función");
                            break;
                        }
                        var selectedContent = editorC.getSelectedHtml().getHtml();

                        selectedWord = selectedContent;
                        list_html = $(elementHtml).parentsUntil("li").parent();

                        window.open(\''; ?>
<?php echo $this->_tpl_vars['url_markup']; ?>
<?php echo '\', \'\', openWindow(500, 600))
                        break;
                    case "cite":
                        var editorC = CKEDITOR.instances.editor1;
                        if (editorC.getSelectedHtml() === null || editorC.getSelectedHtml().getHtml().toString() === "") {
                            alert("Debe seleccionar el texto para realizar esta función");
                            break;
                        }
                        var selectedContent = editorC.getSelectedHtml().getHtml();

                        selectedWord = selectedContent;
                        list_html = $(elementHtml).parentsUntil("li").parent();



                        window.open(\''; ?>
<?php echo $this->_tpl_vars['url_markup']; ?>
<?php echo '\', \'\', openWindow(500, 600))
                        break;
                }

            });
        });

        function openWindow(width, height) {

            var left = parseInt((screen.availWidth / 2) - (width / 2));
            var top = parseInt((screen.availHeight / 2) - (height / 2));
            var windowFeatures = "width=" + width + ",height=" + height + ",status,resizable,left=" + left + ",top=" + top + "screenX=" + left + ",screenY=" + top;

            return windowFeatures;
        }

        function escapeHtml(string) {
            var entityMap = {
                "&": "&amp;",
                "<": "&lt;",
                ">": "&gt;",
                \'"\': \'&quot;\',
                "\'": \'&#39;\',
                //"/": \'&#x2F;\',
                "á": \'&aacute;\',
                "é": \'&eacute;\',
                "í": \'&iacute;\',
                "ó": \'&oacute;\',
                "ú": \'&uacute;\',
                "ñ": \'&ntilde;\',
                "Á": \'&Aacute;\',
                "É": \'&Eacute;\',
                "Í": \'&Iacute;\',
                "O": \'&Oacute;\',
                "Ú": \'&Uacute;\',
                "Ñ": \'&Ntilde;\'
            };
            return String(string).replace(/[áéíóúÁÉÍÓÚ&<>"\'ñÑ]/g, function (s) {
                return entityMap[s];
            });
        }

        function submitAtag(id) {
            $("#" + id + "Html").val(CKEDITOR.instances.editor1.getData());
            $("#" + id).submit();
        }

        function submitExport(id) {
            $("#" + id + "Html").val(CKEDITOR.instances.editor1.getData());
            var answer = confirm(\''; ?>
<?php echo ((is_array($_tmp=$this->_plugins['function']['translate'][0][0]->smartyTranslate(array('key' => "plugins.importexport.PluginPersonalizable.exportScielo"), $this))) ? $this->_run_mod_handler('escape', true, $_tmp, 'jsparam') : $this->_plugins['modifier']['escape'][0][0]->smartyEscape($_tmp, 'jsparam'));?>
<?php echo '\');

            $("#exportScielo").val(answer);

            $("#" + id).submit();
        }

        function getSelectionHtml(editor) {
            var sel = editor.getSelection();
            var ranges = sel.getRanges();
            var el = new CKEDITOR.dom.element("div");
            for (var i = 0, len = ranges.length; i < len; ++i) {
                el.append(ranges[i].cloneContents());
            }
            return el.getHtml();
        }

    '; ?>

</script>
