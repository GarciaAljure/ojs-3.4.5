<?php /* Smarty version 2.6.26, created on 2015-08-27 02:21:13
         compiled from file:C:%5Cwamp%5Cwww%5Cojs/plugins/importexport/pluginPersonalizable/vista/template.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'plugin_url', 'file:C:\\wamp\\www\\ojs/plugins/importexport/pluginPersonalizable/vista/template.tpl', 20, false),array('function', 'translate', 'file:C:\\wamp\\www\\ojs/plugins/importexport/pluginPersonalizable/vista/template.tpl', 21, false),array('block', 'iterate', 'file:C:\\wamp\\www\\ojs/plugins/importexport/pluginPersonalizable/vista/template.tpl', 43, false),array('modifier', 'to_array', 'file:C:\\wamp\\www\\ojs/plugins/importexport/pluginPersonalizable/vista/template.tpl', 47, false),array('modifier', 'escape', 'file:C:\\wamp\\www\\ojs/plugins/importexport/pluginPersonalizable/vista/template.tpl', 48, false),)), $this); ?>
<?php echo ''; ?><?php $this->assign('pageTitle', "plugins.importexport.PluginPersonalizable.selectTemplate"); ?><?php echo ''; ?><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "common/header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?><?php echo ''; ?>





<form action="<?php echo $this->_plugins['function']['plugin_url'][0][0]->smartyPluginUrl(array('path' => 'createTemplate'), $this);?>
" method="post"  >
    <p><?php echo $this->_plugins['function']['translate'][0][0]->smartyTranslate(array('key' => "plugins.importexport.PluginPersonalizable.createTemplateDescription"), $this);?>
</p>
    <input type="submit" value="<?php echo $this->_plugins['function']['translate'][0][0]->smartyTranslate(array('key' => "plugins.importexport.PluginPersonalizable.createTemplate"), $this);?>
" class="button defaultButton "/>
    <br>
    <br>
    <div id="articles">

        <table width="100%" class="listing">
            <tr>
                <td colspan="4" class="headseparator">&nbsp;</td>
            </tr>
            <tr class="heading" valign="bottom">

                <td width="40%"><?php echo $this->_plugins['function']['translate'][0][0]->smartyTranslate(array('key' => "plugins.importexport.PluginPersonalizable.nameTemplate"), $this);?>
</td>
                <td width="40%"><?php echo $this->_plugins['function']['translate'][0][0]->smartyTranslate(array('key' => "plugins.importexport.PluginPersonalizable.dateTemplate"), $this);?>
</td>
                <td width="10%" align="left" ><?php echo $this->_plugins['function']['translate'][0][0]->smartyTranslate(array('key' => "plugins.importexport.PluginPersonalizable.action"), $this);?>
</td>
                <td width="10%" ></td>

            </tr>
            <tr>
                <td colspan="4" class="headseparator">&nbsp;</td>
            </tr>

            <?php $this->_tag_stack[] = array('iterate', array('from' => 'arrayTemplate','item' => 'template')); $_block_repeat=true;$this->_plugins['block']['iterate'][0][0]->smartyIterate($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>
            <tr valign="top">
                <td><?php echo $this->_tpl_vars['template']->getName(); ?>
</td>
                <td><?php echo $this->_tpl_vars['template']->getDate(); ?>
</td>
                <td align="left"><a href="<?php echo $this->_plugins['function']['plugin_url'][0][0]->smartyPluginUrl(array('path' => ((is_array($_tmp='editTemplate')) ? $this->_run_mod_handler('to_array', true, $_tmp, $this->_tpl_vars['template']->getId()) : $this->_plugins['modifier']['to_array'][0][0]->smartyToArray($_tmp, $this->_tpl_vars['template']->getId()))), $this);?>
" class="action"><?php echo $this->_plugins['function']['translate'][0][0]->smartyTranslate(array('key' => "common.edit"), $this);?>
</a></td>
                <td align="left"><a href="<?php echo $this->_plugins['function']['plugin_url'][0][0]->smartyPluginUrl(array('path' => ((is_array($_tmp='deleteTemplate')) ? $this->_run_mod_handler('to_array', true, $_tmp, $this->_tpl_vars['template']->getId()) : $this->_plugins['modifier']['to_array'][0][0]->smartyToArray($_tmp, $this->_tpl_vars['template']->getId()))), $this);?>
"  onclick="return confirm('<?php echo ((is_array($_tmp=$this->_plugins['function']['translate'][0][0]->smartyTranslate(array('key' => "plugins.importexport.PluginPersonalizable.confirmDeleteTemplate"), $this))) ? $this->_run_mod_handler('escape', true, $_tmp, 'jsparam') : $this->_plugins['modifier']['escape'][0][0]->smartyEscape($_tmp, 'jsparam'));?>
')"  class="action"><?php echo $this->_plugins['function']['translate'][0][0]->smartyTranslate(array('key' => "common.delete"), $this);?>
</a></td>

            </tr>

            <tr>
                <td colspan="4" class="separator">&nbsp;</td>
            </tr>
            <?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo $this->_plugins['block']['iterate'][0][0]->smartyIterate($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>

            <?php if (empty ( $this->_tpl_vars['template'] )): ?>
                <tr>
                    <td colspan="4" class="nodata"><?php echo $this->_plugins['function']['translate'][0][0]->smartyTranslate(array('key' => "common.none"), $this);?>
</td>
                </tr>
                <tr>
                    <td colspan="4" class="endseparator">&nbsp;</td>
                </tr>

            <?php endif; ?>


        </table>

    </div>

</form>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "common/footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>