<?php /* Smarty version 2.6.26, created on 2015-08-07 03:29:11
         compiled from file:C:%5Cwamp%5Cwww%5Cojs/plugins/importexport/pluginPersonalizable/vista/editTemplate.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'plugin_url', 'file:C:\\wamp\\www\\ojs/plugins/importexport/pluginPersonalizable/vista/editTemplate.tpl', 17, false),array('function', 'translate', 'file:C:\\wamp\\www\\ojs/plugins/importexport/pluginPersonalizable/vista/editTemplate.tpl', 18, false),)), $this); ?>


<?php echo ''; ?><?php $this->assign('pageTitle', "plugins.importexport.PluginPersonalizable.editTemplate"); ?><?php echo ''; ?><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "common/header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?><?php echo ''; ?>

<form action="<?php echo $this->_plugins['function']['plugin_url'][0][0]->smartyPluginUrl(array('path' => 'saveEditTemplate'), $this);?>
" method="post" id="formEditTree" >
    <p><?php echo $this->_plugins['function']['translate'][0][0]->smartyTranslate(array('key' => "plugins.importexport.PluginPersonalizable.editTemplateDescription"), $this);?>
</p>
    <label><?php echo $this->_plugins['function']['translate'][0][0]->smartyTranslate(array('key' => "plugins.importexport.PluginPersonalizable.name"), $this);?>
</label>
    <input name="name" id="name" type="text" class="textField" value="<?php echo $this->_tpl_vars['template']->getName(); ?>
" />
    <input name="id_template" id="id_template" type="hidden"  value="<?php echo $this->_tpl_vars['template']->getId(); ?>
" />
    <input name="text_html" id="text_html" type="hidden"  />
    <input name="plugin_url" id="plugin_url" type="hidden" value="<?php echo $this->_plugins['function']['plugin_url'][0][0]->smartyPluginUrl(array(), $this);?>
"  />
    <input type="button" value="<?php echo $this->_plugins['function']['translate'][0][0]->smartyTranslate(array('key' => "common.save"), $this);?>
" class="button defaultButton" onclick="copyHtmltoInput()"/>
</form>
<br>
<div class="easy-tree" id="xmlTree">
    <ul id="treeHtml">
        <?php echo $this->_tpl_vars['template']->getText(); ?>

    </ul>
</div>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "common/footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<script >
    <?php echo '
        var elementHtml = null;

        //var nuevo = jQuery.noConflict();
        $(window).load(function () {
            $(\'.easy-tree\').EasyTree({
                addable: true,
                editable: true,
                deletable: true
            });



        });

        function copyHtmltoInput() {
            if ($("#name").val() === "") {
                alert("Debe introducir un nombre para la plantilla");
                return;
            }

            $("#text_html").val($("#treeHtml").html());
            $("#formEditTree").submit();
        }

    '; ?>

</script>
