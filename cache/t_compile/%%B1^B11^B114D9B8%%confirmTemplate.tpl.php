<?php /* Smarty version 2.6.26, created on 2015-08-07 03:30:16
         compiled from file:C:%5Cwamp%5Cwww%5Cojs/plugins/importexport/pluginPersonalizable/vista/confirmTemplate.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'plugin_url', 'file:C:\\wamp\\www\\ojs/plugins/importexport/pluginPersonalizable/vista/confirmTemplate.tpl', 20, false),array('function', 'translate', 'file:C:\\wamp\\www\\ojs/plugins/importexport/pluginPersonalizable/vista/confirmTemplate.tpl', 22, false),array('modifier', 'escape', 'file:C:\\wamp\\www\\ojs/plugins/importexport/pluginPersonalizable/vista/confirmTemplate.tpl', 27, false),)), $this); ?>
<?php echo ''; ?><?php $this->assign('pageTitle', "plugins.importexport.PluginPersonalizable.viewTemplate"); ?><?php echo ''; ?><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "common/header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?><?php echo ''; ?>


<?php $this->assign('contentXml', $this->_tpl_vars['content']); ?>

<br/>

<form action="<?php echo $this->_plugins['function']['plugin_url'][0][0]->smartyPluginUrl(array('path' => 'importTemplate'), $this);?>
" method="post" enctype="multipart/form-data" id="formXml">

    <p><?php echo $this->_plugins['function']['translate'][0][0]->smartyTranslate(array('key' => "plugins.importexport.PluginPersonalizable.import.confirm"), $this);?>
</p>
    <label><?php echo $this->_plugins['function']['translate'][0][0]->smartyTranslate(array('key' => "plugins.importexport.PluginPersonalizable.name"), $this);?>
</label>
    <input name="name" id="name" type="text" class="textField" value="" />

    <input name="plugin_url" id="plugin_url" type="hidden" class="textField" value="<?php echo $this->_plugins['function']['plugin_url'][0][0]->smartyPluginUrl(array(), $this);?>
" />
    <input name="codeXML" id="codeXML" type="hidden" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['content'])) ? $this->_run_mod_handler('escape', true, $_tmp) : $this->_plugins['modifier']['escape'][0][0]->smartyEscape($_tmp)); ?>
" />
    <input name="import" onclick="copyHtmltoInput()" type="button" class="button defaultButton" value="<?php echo $this->_plugins['function']['translate'][0][0]->smartyTranslate(array('key' => "common.confirm"), $this);?>
" />
</form>
<br/>
<br/>
<!--<div class="codeEditor" style="border: 1px solid #76ae76;">
     <pre class="brush: xml;" id="XmlTextarea">
</pre>
</div> -->


<div class="easy-tree" id="xmlTree">
    <ul id="treeHtml">


    </ul>
</div>









<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "common/footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<script >
    <?php echo '
        $("#XmlTextarea").hide();
        //SyntaxHighlighter.all();

        function copyHtmltoInput() {
            if ($("#name").val()==="") {
                alert("Debe introducir un nombre para la plantilla");
                return;
            }

            $("#codeXML").val($("#treeHtml").html());
            $("#formXml").submit();
        }

        $(window).load(function () {
            var html = "";
            $("#treeHtml").append(traverse($.parseXML($("#codeXML").val()), html));
            $(\'.easy-tree\').EasyTree({
                addable: true,
                editable: true,
                deletable: true
            });

            //console.log(traverse($.parseXML($("#codeXML").val()), html));

        });


        /*function traverse(tree, html) {
         if (tree.hasChildNodes()) {
         html = \'<ul><li>\';
         html = tree.tagName;
         console.log(tree.children());
         for (var i = 0; i < tree.children.length; i++)
         {
         return traverse(tree.children(i));
         }
         html = \'</li></ul>\';
         }
         return html;
         }*/



        function traverse(tree, html) {

            $(tree).contents().each(function () {
                if (this.nodeType !== 3) {
                    if (html === "") {
                        html += "<li id=\'startUl\'>" + this.nodeName;
                    } else {
                        html += "<li>" + this.nodeName;
                    }
                    if (this.childElementCount > 0) {
                        html += \'<ul>\';

                        html = traverse(this, html);
                        html += \'</ul>\';
                    }
                    html += "</li>";

                }
            });

            return html;





            /*   $(tree).contents().each(function () {
             if (this.nodeType !== 3) {
             console.log(tree.hasChildNodes());
             
             html += \'<li>\' + this.nodeName + \'</li>\';
             traverse(this, html);
             
             }
             });
             
             
             return html;*/
        }

    '; ?>

</script>
