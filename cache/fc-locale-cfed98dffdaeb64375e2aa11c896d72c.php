<?php return array (
  'plugins.importexport.crossref.displayName' => 'Módulo de exportación CrossRef XML',
  'plugins.importexport.crossref.description' => 'Exportar los metadatos del artículo en formato CrossRef XML.',
  'plugins.importexport.crossref.export' => 'Exportar datos',
  'plugins.importexport.crossref.export.issues' => 'Exportar números',
  'plugins.importexport.crossref.export.selectIssue' => 'Seleccione el número que desea exportar.',
  'plugins.importexport.crossref.export.articles' => 'Exportar artículos',
  'plugins.importexport.crossref.export.selectArticle' => 'Seleccione los artículos que desea exportar.',
  'plugins.importexport.crossref.cliUsage' => 'Uso: {$scriptName} {$pluginName} [xmlFileName] [journal_path] artículos [articleId1] [articleId2] ... {$scriptName} {$pluginName} [xmlFileName] [journal_path] número [issueId]',
  'plugins.importexport.crossref.cliError' => 'ERROR:',
  'plugins.importexport.crossref.export.error.issueNotFound' => 'Ningún número se corresponde con la Id. indicada "{$issueId}".',
  'plugins.importexport.crossref.export.error.articleNotFound' => 'Ningún artículo se corresponde con la Id. indicada "{$articleId}".',
  'plugins.importexport.crossref.errors.noDOIprefix' => 'Para utilizar este módulo vaya a la categoría del módulo del "Identificador público", active y configure el módulo DOI e indique un prefijo del DOI válido.',
); ?>