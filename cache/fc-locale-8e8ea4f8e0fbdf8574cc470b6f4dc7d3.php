<?php return array (
  'plugins.importexport.PluginPersonalizable.displayName' => 'Módulo de importación/exportación de marcado de texto en XML mediante plantillas personalizables',
  'plugins.importexport.PluginPersonalizable.description' => 'Exportar e importar el texto marcado de un artículo en XML.',
  'plugins.importexport.PluginPersonalizable.selectElement' => 'Seleccionar elemento',
  'plugins.importexport.PluginPersonalizable.Article' => 'Artículos',
  'plugins.importexport.PluginPersonalizable.Issue' => 'Números',
  'plugins.importexport.PluginPersonalizable.Template' => 'Plantillas',
  'plugins.importexport.PluginPersonalizable.confirmDeleteTemplate' => '¿Realmente desea eliminar la plantilla?',
  'plugins.importexport.PluginPersonalizable.selectArticle.long' => 'Seleccionar artículo',
  'plugins.importexport.PluginPersonalizable.selectIssue.short' => 'Módulo de importación/exportación de marcado de texto en XML mediante plantillas personalizables',
  'plugins.importexport.PluginPersonalizable.selectIssues' => 'Seleccionar número',
  'plugins.importexport.PluginPersonalizable.issue' => 'NÚMERO',
  'plugins.importexport.PluginPersonalizable.title' => 'TÍTULO',
  'plugins.importexport.PluginPersonalizable.autor' => 'AUTORES/AS',
  'plugins.importexport.PluginPersonalizable.action' => 'ACCIÓN',
  'plugins.importexport.PluginPersonalizable.element' => 'ELEMENTOS',
  'plugins.importexport.PluginPersonalizable.date' => 'FECHA DE CREACIÓN',
  'plugins.importexport.PluginPersonalizable.next' => 'Guardar y continuar',
  'plugins.importexport.PluginPersonalizable.save' => 'Guardar y salir',
  'plugins.importexport.PluginPersonalizable.exportIssue' => 'Exportar número',
  'plugins.importexport.PluginPersonalizable.exportScielo' => '¿Desea darle el tratamiento a los archivos de acuerdo a la normativa de Scielo?',
  'plugins.importexport.PluginPersonalizable.editorText' => 'A continuación encontrará el contenido del artículo en el editor de texto',
  'plugins.importexport.PluginPersonalizable.selectTemplate' => 'Seleccionar plantilla',
  'plugins.importexport.PluginPersonalizable.selectTemplateDescription' => 'Este módulo permite seleccionar la plantilla que se va a utilizar para
        realizar el marcado de texto de los artículos.',
  'plugins.importexport.PluginPersonalizable.selectIssueTemplateDescription' => 'Este módulo permite exportar el marcado de texto realizado teniendo como base 
        la plantilla a seleccionar.',
  'plugins.importexport.PluginPersonalizable.nameTemplate' => 'NOMBRE',
  'plugins.importexport.PluginPersonalizable.dateTemplate' => 'FECHA DE CREACIÓN',
  'plugins.importexport.PluginPersonalizable.createTemplate' => 'Crear plantilla',
  'plugins.importexport.PluginPersonalizable.createTemplateDescription' => 'Mediante ese módulo puede
        gestionar las plantillas creadas a través de opciones presentadas en la lista que se despliegua a continuación, además 
        se permite la creación de una nueva plantilla.',
  'plugins.importexport.PluginPersonalizable.titleTemplate' => 'Creación de plantilla de marcado de texto',
  'plugins.importexport.PluginPersonalizable.viewTemplate' => 'Visualización de plantilla importada',
  'plugins.importexport.PluginPersonalizable.editTemplate' => 'Edición de plantilla',
  'plugins.importexport.PluginPersonalizable.editTemplateDescription' => 'Edite la plantilla a su gusto 
        y oprima el botón guardar para registrar los cambios realizados',
  'plugins.importexport.PluginPersonalizable.import' => 'Importar plantilla',
  'plugins.importexport.PluginPersonalizable.import.description' => 'Este módulo admite la importación de plantillas para realizar el marcado de texto de articulos. 
        Es importante mencionar que el archivo a importar deber ser .xml',
  'plugins.importexport.PluginPersonalizable.createTree' => 'Creación de plantillas a través de un árbol jerárquico',
  'plugins.importexport.PluginPersonalizable.createTree.description' => 'Este módulo permite la creación de un árbol jerárquico de etiquetas XML, las cuales 
        representarán una plantilla de marcado de texto. Por favor diseñe el árbol jerárquico, digite el nombre 
        y oprima el botón confirmar para registrar exitosamente la plantilla',
  'plugins.importexport.PluginPersonalizable.name' => 'Nombre',
  'plugins.importexport.PluginPersonalizable.import.confirm' => 'Este módulo permite la visualización de la plantilla importada, por favor digite el nombre 
        y oprima el botón confirmar para registrar exitosamente la plantilla',
  'plugins.importexport.PluginPersonalizable.import.successful' => 'Plantilla registrada exitosamente',
  'plugins.importexport.PluginPersonalizable.import.successful.description' => 'La plantilla fue guardada exitosamente
        en la base de datos',
  'plugins.importexport.PluginPersonalizable.import.wrong' => 'Error al registrar la plantilla',
  'plugins.importexport.PluginPersonalizable.import.wrong.description' => 'Se presentaron problemas al
        registrar la plantilla, contacte al administrador para solucioanr los problemas',
  'plugins.importexport.PluginPersonalizable.import.error' => 'Error de importación',
  'plugins.importexport.PluginPersonalizable.import.error.description' => 'Se presentaron problemas al realizar la
        importación, verifique que haya seleccionado un archivo o verifique que haya seleccionado un archivo correcto',
  'plugins.importexport.PluginPersonalizable.markup.error' => 'Error de exportación de archivos de marcado de texto',
  'plugins.importexport.PluginPersonalizable.markup.error.description' => 'Se presentaron problemas al realizar la
        exportación, verifique que haya marcado los artículos del número escogido con la plantilla seleccionada',
  'plugins.importexport.PluginPersonalizable.copySelectedText' => 'Seleccione el texto del editor, luego
        vaya al árbol de etiquetas y asignele al texto seleccionado una etiqueta',
  'plugins.importexport.PluginPersonalizable.textEditor' => 'Editor de texto',
  'plugins.importexport.PluginPersonalizable.Tree' => 'Árbol de etiquetas',
  'plugins.importexport.PluginPersonalizable.asignTree' => 'Asigne la etiqueta al texto seleccionado dando click derecho en el nodo deseado',
  'plugins.importexport.PluginPersonalizable.AsignTag' => 'Asignar etiqueta',
  'plugins.importexport.PluginPersonalizable.markupAuthor' => 'Marcar autor',
  'plugins.importexport.PluginPersonalizable.markupCite' => 'Marcar referencia',
  'plugins.importexport.PluginPersonalizable.export' => 'Exportar datos',
  'plugins.importexport.PluginPersonalizable.export.issues' => 'Exportar números',
  'plugins.importexport.PluginPersonalizable.export.selectIssue' => 'Seleccione el número que desea exportar.',
  'plugins.importexport.PluginPersonalizable.export.articles' => 'Exportar artículos',
  'plugins.importexport.PluginPersonalizable.export.selectArticle' => 'Seleccione los artículos que desea exportar.',
  'plugins.importexport.PluginPersonalizable.export.markup' => 'Marcado de texto del artículo',
  'plugins.importexport.PluginPersonalizable.cliUsage' => 'Uso: {$scriptName} {$pluginName} [xmlFileName] [journal_path] artículos [articleId1] [articleId2] ... {$scriptName} {$pluginName} [xmlFileName] [journal_path] número [issueId]',
  'plugins.importexport.PluginPersonalizable.cliError' => 'ERROR:',
  'plugins.importexport.PluginPersonalizable.export.error.issueNotFound' => 'Ningún número se corresponde con la Id. indicada "{$issueId}".',
  'plugins.importexport.PluginPersonalizable.export.error.articleNotFound' => 'Ningún artículo se corresponde con la Id. indicada "{$articleId}".',
  'plugins.importexport.PluginPersonalizable.errors.noDOIprefix' => 'Para utilizar este módulo vaya a la categoría del módulo del "Identificador público", active y configure el módulo DOI e indique un prefijo del DOI válido.',
); ?>