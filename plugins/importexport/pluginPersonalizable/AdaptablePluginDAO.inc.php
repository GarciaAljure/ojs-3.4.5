<?php

/**
 * @file AdaptablePluginDAO.inc.php
 *
 * Copyright (c) 2000-2008 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @class AdaptablePluginDAO
 * @ingroup plugins_import/export
 *
 * @brief 
 */
import('db.DAO');

class AdaptablePluginDAO extends DAO {

    /**
     * this function allows insert in database the templates
     * @param TemplateMarkup - object 
     * @return answer boleean
     */
    function registerTemplate($templateMarkup) {

        $ret = $this->update(
                sprintf('INSERT INTO template (name, date, text ) VALUES (?, ?, ?)'), array($templateMarkup->getName(), $templateMarkup->getDate(), $templateMarkup->getText())
        );

        return $ret;
    }

    /**
     * this function allows select all of the template, converting everything in
     * object
     * @return object
     */
    function getTemplates() {
        $sql = 'SELECT * FROM template ';
        $params = false;
        $result = & $this->retrieve($sql, $params);
        $array = null;

        while (!$result->EOF) {
            $row = $result->GetRowAssoc(false);

            $array[] = $this->returnTemplateFromRow($row);
            $result->moveNext();
        }

        return $array;
    }

    /**
     * this function allows get one template
     * @param $idTemplate 
     * @return object
     */
    function getTemplate($idTemplate) {
        $sql = 'SELECT * FROM template WHERE id = ? ';
        $params = $idTemplate;
        $result = & $this->retrieve($sql, $params);
        $array = null;

        while (!$result->EOF) {
            $row = $result->GetRowAssoc(false);

            $array = $this->returnTemplateFromRow($row);
            $result->moveNext();
        }

        return $array;
    }

    /**
     * this function allows update templates
     * @param  $templateMarkup
     * @return Boolean
     */
    function updateTemplate($templateMarkup) {
        $ret = $this->update(
                sprintf('UPDATE template set name=? , text=? where id=?'), array($templateMarkup->getName(), $templateMarkup->getText(), $templateMarkup->getId()));

        return $ret;
    }

    /**
     * through this function the templete is deleted
     * @param $id
     * @return Boolean
     */
    function deleteTemplate($id) {
        $ret = $this->update(
                sprintf('DELETE FROM template where id=?'), array($id));

        return $ret;
    }

    /**
     * this function allows get number of markup article
     * @param  $idVolume
     * @param  $idIssue
     * @param  $idArticle
     * @param  $idTemplate
     * @return Int
     */
    function getNumberMarkup($idVolume, $idIssue, $idArticle, $idTemplate) {

        $sql = 'SELECT COUNT(*) FROM markup WHERE id_volume = ? AND id_issue=? AND id_article=? AND id_template=? ';
        $params = array($idVolume, $idIssue, $idArticle, $idTemplate);
        $result = & $this->retrieve($sql, $params);
        return $result->fields[0] > 0 ? true : false;
    }

    /**
     * this funcion allows insert into database the markup article
     * @param $textMarkup
     * @return Boolean
     */
    function saveMarkup($textMarkup) {

        $ret = $this->update(
                sprintf('INSERT INTO markup (id_volume,id_issue,id_article,id_template,html,markuphtml) VALUES (?, ?, ?, ?,?, ?)'), array($textMarkup->getIdVolume(), $textMarkup->getIdIssue(), $textMarkup->getIdArticle(), $textMarkup->getIdTemplate(), $textMarkup->getHtml(), $textMarkup->getMarkupHtml())
        );

        return $ret;
    }

    /**
     * this function allows udpate the markup into databse
     * @param type $textMarkup
     * @return type
     */
    function updateMarkup($textMarkup) {
        $ret = $this->update(
                sprintf('UPDATE markup set html=? , markuphtml=? WHERE id_volume = ? AND id_issue=? AND id_article=? AND id_template=?'), array($textMarkup->getHtml(), $textMarkup->getMarkupHtml(), $textMarkup->getIdVolume(), $textMarkup->getIdIssue(), $textMarkup->getIdArticle(), $textMarkup->getIdTemplate()));

        return $ret;
    }

    /**
     * this function allows get markup through parameters
     * @param  $volume
     * @param  $issue
     * @param  $article
     * @param  $template
     * @return Object
     */
    function getMarkup($volume, $issue, $article, $template) {
        $sql = 'SELECT html,markuphtml FROM markup WHERE id_volume = ? AND id_issue=? AND id_article=? AND id_template=?';
        $params = array($volume, $issue, $article, $template);
        $result = & $this->retrieve($sql, $params);
        $array = null;


        while (!$result->EOF) {
            $row = $result->GetRowAssoc(false);
            $array = $this->returnMarkupFromRow($row);
            $result->moveNext();
        }

        return $array;
    }

    /**
     * this fucntion allows convert the result from database in object
     * @param  $row
     * @return TemplateMarkup
     */
    function returnTemplateFromRow($row) {
        $template = new TemplateMarkup();
        $template->setId($row['id']);
        $template->setName($row['name']);
        $template->setDate($row['date']);
        $template->setText($row['text']);
        return $template;
    }

    /**
     * this function allows convert the result from database in object
     * @param $row
     * @return TextMarkup
     */
    function returnMarkupFromRow($row) {
        $textMarkup = new TextMarkup();
        $textMarkup->setHtml($row['html']);
        $textMarkup->setMarkupHtml($row['markuphtml']);

        return $textMarkup;
    }

}
