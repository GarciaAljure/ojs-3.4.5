<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TextMarkup
 *
 * @author CarlosAndres
 */
class TextMarkup {

    private $idVolume;
    private $idIssue;
    private $idArticle;
    private $idTemplate;
    private $html;
    private $markupHtml;

    function __construct() {
        
    }

    function getIdVolume() {
        return $this->idVolume;
    }

    function getIdIssue() {
        return $this->idIssue;
    }

    function getIdArticle() {
        return $this->idArticle;
    }

    function getIdTemplate() {
        return $this->idTemplate;
    }

    function getHtml() {
        return $this->html;
    }

    function getMarkupHtml() {
        return $this->markupHtml;
    }

    function setIdVolume($idVolume) {
        $this->idVolume = $idVolume;
    }

    function setIdIssue($idIssue) {
        $this->idIssue = $idIssue;
    }

    function setIdArticle($idArticle) {
        $this->idArticle = $idArticle;
    }

    function setIdTemplate($idTemplate) {
        $this->idTemplate = $idTemplate;
    }

    function setHtml($html) {
        $this->html = $html;
    }

    function setMarkupHtml($markupHtml) {
        $this->markupHtml = $markupHtml;
    }

}
