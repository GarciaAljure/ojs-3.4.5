<?php

/**
 * @defgroup plugins_importexport_pluginpersonalizable
 */
 
/**
 * @file plugins/importexport/pluginPersonalizable/index.php
 *
 * Copyright (c) 2015 <Carlos Garcia>
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @ingroup plugins_importexport_pluginPersonalizable
 * @brief 
 *
 */

require_once('AdaptablePlugin.inc.php');

return new AdaptablePlugin();

