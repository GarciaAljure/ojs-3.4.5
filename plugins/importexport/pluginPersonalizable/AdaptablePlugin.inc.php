<?php

/**
 * @file plugins/importexport/AdaptablePlugin.inc.php
 *
 * Copyright (c) 2013-2014 Simon Fraser University Library
 * Copyright (c) 2003-2014 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @class AdaptablePlugin
 * @ingroup plugins_importexport_AdaptablePlugin
 *
 * @brief AdaptablePlugin import/export plugin
 */
import('classes.plugins.ImportExportPlugin');
import('classes.file.TemporaryFileManager');
import('classes.session.Session');

require 'controlador/Parser.php';
require 'controlador/Converter.php';
require 'controlador/Remover.php';
require 'modelo/TemplateMarkup.php';
require 'modelo/TextMarkup.php';
require 'lib/PdfaidServices.php';

class AdaptablePlugin extends ImportExportPlugin {

    private $temporaryFileManager;
    private $array_idArticle;

    /**
     * Called as a plugin is registered to the registry
     * @param $category String Name of category plugin was registered to
     * @return boolean True iff plugin initialized successfully; if false,
     * 	the plugin will not be registered.
     */
    function register($category, $path) {
        $success = parent::register($category, $path);
// Additional registration / initialization code
// should go here. For example, load additional locale data:
        $this->addLocaleData();

        $this->import('AdaptablePluginDAO');
        $adaptablePluginDAO = & new AdaptablePluginDAO();
        $ret = & DAORegistry::registerDAO('AdaptablePluginDAO', $adaptablePluginDAO);


// This is fixed to return false so that this coding sample
// isn't actually registered and displayed. If you're using
// this sample for your own code, make sure you return true
// if everything is successfully initialized.
        return $success;
    }

    /**
     * Get the name of this plugin. The name must be unique within
     * its category.
     * @return String name of plugin
     */
    function getName() {
// This should not be used as this is an abstract class
        return 'PluginPersonalizable';
    }

    function getDisplayName() {
        return __('plugins.importexport.PluginPersonalizable.displayName');
    }

    function getDescription() {
        return __('plugins.importexport.PluginPersonalizable.description');
    }

    function display(&$args, $request) {
        parent::display($args, $request);
        $temporaryFileManager = new TemporaryFileManager();
        $journal = & Request::getJournal();
        $issueDao = & DAORegistry::getDAO('IssueDAO');
        $publishedArticleDao = & DAORegistry::getDAO('PublishedArticleDAO');
        $adaptablePluginDAO = & DAORegistry::getDAO('AdaptablePluginDAO');

        switch (array_shift($args)) {
            //this case allows list issues
            case 'issues':
                $this->setBreadcrumbs(array(), true);
                $templateMgr = & TemplateManager::getManager();
                AppLocale::requireComponents(LOCALE_COMPONENT_OJS_EDITOR);
                $issues = & $issueDao->getIssues($journal->getId(), Handler::getRangeInfo('issues'));
                $templateMgr->assign_by_ref('issues', $issues);
                $templateMgr->display($this->getTemplatePath() . 'vista/issues.tpl');
                break;
            //this case allows list templates
            case 'template':
                $this->setBreadcrumbs(array(), true);
                $templateMgr = & TemplateManager::getManager();
                $result = $adaptablePluginDAO->getTemplates();
                import('lib.pkp.classes.core.VirtualArrayIterator');
                $iterator = new VirtualArrayIterator($result, count($result), -1, -1);
                $pathPlugin = $this->getPluginPath() . '/';
                $templateMgr->addJavaScript($pathPlugin . 'js/jquery.min.js');
                $templateMgr->assign_by_ref('arrayTemplate', $iterator);
                $templateMgr->display($this->getTemplatePath() . 'vista/template.tpl');
                break;
            //this case allows format URL(0 if there is another article, 1 otherwise)
            //this case is called when a issue is selected for first time
            case 'selectIssue':
                $this->setBreadcrumbs(array(), true);
                $issueId = array_shift($args);
                $articles = $publishedArticleDao->getPublishedArticles($issueId);
                $url = "../selectArticle" . "/" . $issueId;
                for ($index = 0; $index < count($articles); $index++) {
                    $url .="/" . $articles[$index]->getArticleId();
                    if (($index + 1) !== count($articles)) {
                        $url .="/0";
                    } else {
                        $url .="/1";
                    }
                    break;
                }
                $request->redirectUrl($url);
                break;
            //this case is called from markup.tpl when the article must be saved
            case 'selectedArticle':
                $this->setBreadcrumbs(array(), true);
                $issueId = $request->getUserVar('issueId');
                $articleId = $request->getUserVar('articleId');
                $templateId = $request->getUserVar('templateId');
                $html = $request->getUserVar('selectedArticleHtml');
                $markupHtml = $request->getUserVar('articleListHtml');
                $newArticleId = "";
                $volumeId = $issueDao->getIssueById($issueId)->getVolume();
                $textMarkup = new TextMarkup();
                $textMarkup->setIdVolume($volumeId);
                $textMarkup->setIdIssue($issueId);
                $textMarkup->setIdArticle($articleId);
                $textMarkup->setIdTemplate($templateId);
                $textMarkup->setHtml($html);
                $textMarkup->setMarkupHtml($markupHtml);
                $result = $adaptablePluginDAO->getNumberMarkup($volumeId, $issueId, $articleId, $templateId);
                if ($result) {
                    $result = $adaptablePluginDAO->updateMarkup($textMarkup);
                } else {
                    $result = $adaptablePluginDAO->saveMarkup($textMarkup);
                }
                $articles = $publishedArticleDao->getPublishedArticles($issueId);
                $falseId = 0;
                for ($index = 0; $index < count($articles); $index++) {
                    if ($articles[$index]->getArticleId() === $articleId) {
                        if (($index + 1) < count($articles)) {
                            $newArticleId = $articles[$index + 1]->getArticleId();
                            if (($index + 2) === count($articles)) {
                                $falseId = 1;
                            }
                        } else {
                            $newArticleId = 0;
                            $falseId = 1;
                        }
                    }
                }
                $url = "selectArticle" . "/" . $issueId . "/" . $newArticleId . "/" . $falseId;
                $request->redirectUrl($url);
                break;
            //through this case the article is converted to html and redirect to 
            // markup.tpl
            case 'selectArticle':
                $this->setBreadcrumbs(array(), true);
                $issueId = array_shift($args);
                $artId = array_shift($args);
                $articleId = array($artId);
                $falseId = array_shift($args);
                $sessionManager = SessionManager::getManager();
                $session = $sessionManager->getUserSession();
                $idTemplate = $session->getSessionVar('idTemplate');
                $result = ArticleSearch::formatResults($articleId);
                if (count($result) < 1) {
                    break;
                }
                $fileId = $result[0]["article"]->_data["submissionFileId"];
                import('classes.file.ArticleFileManager');
                $articleFileManager = new ArticleFileManager($result[0]["article"]->getArticleId());
                $fileContent = & $articleFileManager->getFile($fileId, null);
                $articles = $publishedArticleDao->getPublishedArticles($issueId);
                $article = 0;
                for ($index = 0; $index < count($articles); $index++) {
                    if ($articles[$index]->getArticleId() === $artId) {
                        $article = $index + 1;
                    }
                }
                $basePath = Request::getBaseUrl() . "/" . $this->getPluginPath();
                $pluginPath = $this->getPluginPath();
                $volumeId = $issueDao->getIssueById($issueId)->getVolume();
                $resultDao = $adaptablePluginDAO->getMarkup($volumeId, $issueId, $artId, $idTemplate);
                $htmlNoMarkup = "";
                $htmlMarkup = "";

                if ($resultDao == null) {
                    if ($result[0]["publishedArticle"]->getGalleys()[0]->isPdfGalley()) {
                        $nameFile = "pdf2doc.docx";
                        Parser::parserPdfToDocxl($fileContent->getFilePath(), sys_get_temp_dir() . DIRECTORY_SEPARATOR, $nameFile);
                        $htmlNoMarkup = Parser::parserDocxToHtml(sys_get_temp_dir() . DIRECTORY_SEPARATOR . $nameFile, $basePath, $pluginPath, $volumeId, $issueId, $article);
                    } else {
                        $htmlNoMarkup = Parser::parserDocxToHtml($fileContent->getFilePath(), $basePath, $pluginPath, $volumeId, $issueId, $article);
                    }

                    $htmlNoMarkup = Remover::removeEmptyTagsRecursive($htmlNoMarkup);
                    $htmlNoMarkup = Remover::removeRepetitiveTags($htmlNoMarkup);
					$htmlNoMarkup=str_replace("SEQ Tabla \* ARABIC", "", str_replace("SEQ Figura \* ARABIC", "", $htmlNoMarkup));
                    //$htmlNoMarkup = str_replace($result[0]["article"]->getArticleTitle(), htmlspecialchars("<title>", ENT_QUOTES) . $result[0]["article"]->getArticleTitle() . htmlspecialchars("</title>", ENT_QUOTES), $htmlNoMarkup);
                    //$htmlNoMarkup = str_replace($result[0]["article"]->getArticleAbstract(), htmlspecialchars("<abstract>", ENT_QUOTES) . $result[0]["article"]->getArticleAbstract() . htmlspecialchars("</abstract>", ENT_QUOTES), $htmlNoMarkup);
                    $htmlMarkup = $htmlNoMarkup;
                } else {
                    $htmlNoMarkup = $resultDao->getHtml();
                    $htmlMarkup = $resultDao->getMarkupHtml();
                }
                $templateMgr = & TemplateManager::getManager();
                $template = $adaptablePluginDAO->getTemplate($idTemplate);
                $templateMgr->assign('issueId', $issueId);
                $templateMgr->assign('articleId', $artId);
                $templateMgr->assign('falseId', $falseId);
                $templateMgr->assign('template', $template);
                $templateMgr->assign('htmlMarkup', $htmlMarkup);
                $templateMgr->assign('htmlNoMarkup', $htmlNoMarkup);
                $templateMgr->assign('url_markup', Request::getBaseUrl() . "/" . $this->getPluginPath() . "/" . 'vista' . "/" . "markupDialog.php");
                $pathPlugin = $this->getPluginPath() . "/";
                $templateMgr->addJavaScript($pathPlugin . 'js/jquery.min.js');
                $templateMgr->addJavaScript($pathPlugin . 'lib/Bootstrap/bootstrap.min.js');
                $templateMgr->addJavaScript($pathPlugin . 'lib/ckeditor/ckeditor.js');
                $templateMgr->addJavaScript($pathPlugin . 'lib/ckeditor/adapters/jquery.js');
                $templateMgr->addJavaScript($pathPlugin . 'lib/EasyTree/src/easyTree.js');
                $templateMgr->addJavaScript($pathPlugin . 'lib/TableExport/tableExport.js');
                $templateMgr->addJavaScript($pathPlugin . 'lib/TableExport/jquery.base64.js');
                $templateMgr->addJavaScript($pathPlugin . 'lib/TableExport/html2canvas.js');
                $templateMgr->addStyleSheet(Request::getBaseUrl() . "/" . $this->getPluginPath() . "/" . 'lib' . "/" . 'EasyTree' . "/" . 'css' . "/" . 'easyTree.css');
                $templateMgr->addStyleSheet(Request::getBaseUrl() . "/" . $this->getPluginPath() . "/" . 'lib' . "/" . 'Bootstrap' . "/" . 'bootstrap.css');
                $templateMgr->addStyleSheet(Request::getBaseUrl() . "/" . $this->getPluginPath() . "/" . 'css' . "/" . 'styleMarkup.css');
                $templateMgr->display($this->getTemplatePath() . 'vista/markup.tpl');
                break;
            //this case allows get the file imported and redirect to see the xml tree
            case 'import':
                $this->setBreadcrumbs(array(), true);
                $templateMgr = & TemplateManager::getManager();
                $user = & $request->getUser();
                $existingFileId = $request->getUserVar('temporaryFileId');
                $temporaryFile = "";
                if ($existingFileId) {
                    $temporaryFile = $temporaryFileManager->getFile($existingFileId, $user->getId());
                } else {
                    $temporaryFile = $temporaryFileManager->handleUpload('importFile', $user->getId());
                }
                $fileContent = $temporaryFileManager->readFile($temporaryFile->getId(), $user->getId(), false);

                if ($temporaryFile && $fileContent && simplexml_load_string($fileContent) && $fileContent !== "") {
                    $pathPlugin = $this->getPluginPath() . '/';
                    $templateMgr->addJavaScript($pathPlugin . 'js/jquery.min.js');
                    $templateMgr->addJavaScript($pathPlugin . 'lib/Bootstrap/bootstrap.min.js');
                    $templateMgr->addJavaScript($pathPlugin . 'lib/EasyTree/src/easyTree.js');
                    $templateMgr->addStyleSheet(Request::getBaseUrl() . "/" . $this->getPluginPath() . "/" . 'lib' . "/" . 'EasyTree' . "/" . 'css' . "/" . 'easyTree.css');
                    $templateMgr->addStyleSheet(Request::getBaseUrl() . "/" . $this->getPluginPath() . "/" . 'lib' . "/" . 'Bootstrap' . "/" . 'bootstrap.css');
                    $templateMgr->addStyleSheet(Request::getBaseUrl() . "/" . $this->getPluginPath() . "/" . 'css' . "/" . 'styleMarkup.css');
                    $templateMgr->assign('content', $fileContent);
                    $templateMgr->display($this->getTemplatePath() . 'vista/confirmTemplate.tpl');
                } else {
                    $templateMgr->display($this->getTemplatePath() . 'vista/errorTemplate.tpl');
                }
                break;
            //through this case the template is created
            case 'importTemplate':
                $this->setBreadcrumbs(array(), true);
                $templateMgr = & TemplateManager::getManager();
                $codeXml = $request->getUserVar('codeXML');
                $name = $request->getUserVar('name');
                $plugin_url = $request->getUserVar('plugin_url');
                date_default_timezone_set("America/Bogota");
                $date = date("Y-m-d H:i:s");
                $templateMarkup = new TemplateMarkup();
                $templateMarkup->setName($name);
                $templateMarkup->setDate($date);
                $templateMarkup->setText($codeXml);
                $success = $adaptablePluginDAO->registerTemplate($templateMarkup);
                $pathPlugin = $this->getPluginPath() . '/';
                $templateMgr->addJavaScript($pathPlugin . 'js/jquery.min.js');
                if ($success) {
                    $request->redirectUrl($plugin_url . "template");
                } else {
                    $templateMgr->display($this->getTemplatePath() . 'vista/wrongTemplate.tpl');
                }
                break;
            //this case allows see the page to create the template
            case 'createTemplate':
                $this->setBreadcrumbs(array(), true);
                $templateMgr = & TemplateManager::getManager();
                $pathPlugin = $this->getPluginPath() . '/';
                $templateMgr->addJavaScript($pathPlugin . 'js/jquery.min.js');
                $templateMgr->addJavaScript($pathPlugin . 'lib/Bootstrap/bootstrap.min.js');
                $templateMgr->addJavaScript($pathPlugin . 'lib/EasyTree/src/easyTree.js');
                $templateMgr->addStyleSheet(Request::getBaseUrl() . "/" . $this->getPluginPath() . "/" . 'lib' . "/" . 'EasyTree' . "/" . 'css' . "/" . 'easyTree.css');
                $templateMgr->addStyleSheet(Request::getBaseUrl() . "/" . $this->getPluginPath() . "/" . 'lib' . "/" . 'Bootstrap' . "/" . 'bootstrap.css');
                $templateMgr->addStyleSheet(Request::getBaseUrl() . "/" . $this->getPluginPath() . "/" . 'css' . "/" . 'styleMarkup.css');
                $templateMgr->display($this->getTemplatePath() . 'vista/templates.tpl');
                break;
            //this case allows save the template
            case 'CreateTreeTemplate':
                $this->setBreadcrumbs(array(), true);
                $templateMgr = & TemplateManager::getManager();
                $codeHtml = $request->getUserVar('html');
                $name = $request->getUserVar('name');
                $plugin_url = $request->getUserVar('plugin_url');
                date_default_timezone_set("America/Bogota");
                $date = date("Y-m-d H:i:s");
                $templateMarkup = new TemplateMarkup();
                $templateMarkup->setName($name);
                $templateMarkup->setDate($date);
                $templateMarkup->setText($codeHtml);
                $success = $adaptablePluginDAO->registerTemplate($templateMarkup);
                if ($success) {
                    $request->redirectUrl($plugin_url . "template");
                } else {
                    $templateMgr->display($this->getTemplatePath() . 'vista/wrongTemplate.tpl');
                }
                break;
            //this case allows list the templates
            case 'SelectTemplate':
                $this->setBreadcrumbs(array(), true);
                $issueId = array_shift($args);
                $templateMgr = & TemplateManager::getManager();
                $result = $adaptablePluginDAO->getTemplates();
                import('lib.pkp.classes.core.VirtualArrayIterator');
                $iterator = new VirtualArrayIterator($result, count($result), -1, -1);
                $pathPlugin = $this->getPluginPath() . '/';
                $templateMgr->addJavaScript($pathPlugin . 'js/jquery.min.js');
                $templateMgr->assign_by_ref('arrayTemplate', $iterator);
                $templateMgr->assign_by_ref('issueId', $issueId);
                $templateMgr->display($this->getTemplatePath() . 'vista/SelectTemplate.tpl');
                break;
            //this case is called from SelecTemplate.tpl
            case 'SelectedTemplate':
                $this->setBreadcrumbs(array(), true);
                $issueId = array_shift($args);
                $TemplateId = array_shift($args);
                $sessionManager = SessionManager::getManager();
                $session = $sessionManager->getUserSession();
                $session->setSessionVar("idTemplate", $TemplateId);
                $url = "../../selectIssue" . "/" . $issueId;
                $request->redirectUrl($url);
                break;
            //this case allows see the page to edit the template
            case 'editTemplate':
                $this->setBreadcrumbs(array(), true);
                $TemplateId = array(array_shift($args));
                $templateMgr = & TemplateManager::getManager();
                $template = $adaptablePluginDAO->getTemplate($TemplateId);
                $pathPlugin = $this->getPluginPath() . '/';
                $templateMgr->addJavaScript($pathPlugin . 'js/jquery.min.js');
                $templateMgr->addJavaScript($pathPlugin . 'lib/Bootstrap/bootstrap.min.js');
                $templateMgr->addJavaScript($pathPlugin . 'lib/EasyTree/src/easyTree.js');
                $templateMgr->addStyleSheet(Request::getBaseUrl() . "/" . $this->getPluginPath() . "/" . 'lib' . "/" . 'EasyTree' . "/" . 'css' . "/" . 'easyTree.css');
                $templateMgr->addStyleSheet(Request::getBaseUrl() . "/" . $this->getPluginPath() . "/" . 'lib' . "/" . 'Bootstrap' . "/" . 'bootstrap.css');
                $templateMgr->addStyleSheet(Request::getBaseUrl() . "/" . $this->getPluginPath() . "/" . 'css' . "/" . 'styleMarkup.css');
                $templateMgr->assign('template', $template);
                $templateMgr->display($this->getTemplatePath() . 'vista/editTemplate.tpl');
                break;
            //this case allows save the template after the edition
            case 'saveEditTemplate':
                $this->setBreadcrumbs(array(), true);
                $codeHtml = $request->getUserVar('text_html');
                $name = $request->getUserVar('name');
                $TemplateId = $request->getUserVar('id_template');
                $plugin = $request->getUserVar('plugin_url');
                $templateMgr = & TemplateManager::getManager();
                $templateMarkup = new TemplateMarkup();
                $templateMarkup->setId($TemplateId);
                $templateMarkup->setName($name);
                $templateMarkup->setText($codeHtml);
                $template = $adaptablePluginDAO->updateTemplate($templateMarkup);
                $request->redirectUrl($plugin . "template");
                break;
            //through this case the markup is saved (no export)
            case 'saveAndExitMarkup':
                $this->setBreadcrumbs(array(), true);
                $issueId = $request->getUserVar('issueId');
                $articleId = $request->getUserVar('articleId');
                $templateId = $request->getUserVar('templateId');
                $html = $request->getUserVar('saveAndExitMarkupHtml');
                $markupHtml = $request->getUserVar('saveAndExitHtml');
                $volumeId = $issueDao->getIssueById($issueId)->getVolume();
                $textMarkup = new TextMarkup();
                $textMarkup->setIdVolume($volumeId);
                $textMarkup->setIdIssue($issueId);
                $textMarkup->setIdArticle($articleId);
                $textMarkup->setIdTemplate($templateId);
                $textMarkup->setHtml($html);
                $textMarkup->setMarkupHtml($markupHtml);
                $result = $adaptablePluginDAO->getNumberMarkup($volumeId, $issueId, $articleId, $templateId);
                if ($result) {
                    $result = $adaptablePluginDAO->updateMarkup($textMarkup);
                } else {
                    $result = $adaptablePluginDAO->saveMarkup($textMarkup);
                }
                $url = "issues";
                $request->redirectUrl($url);
                break;
            //this case allows ecport one issue
            case 'exportIssueMarkup':
                $this->setBreadcrumbs(array(), true);
                $this->exportIssue($request, $journal);
                break;
            //this case allows see and select issues
            case 'exportIssues':
                $this->setBreadcrumbs(array(), true);
                $issueIds = $request->getUserVar('issueId');
                if (!isset($issueIds)) {
                    $issueIds = array();
                }
                $sessionManager = SessionManager::getManager();
                $session = $sessionManager->getUserSession();
                $session->setSessionVar("Issues", $issueIds);
                $templateMgr = & TemplateManager::getManager();
                $result = $adaptablePluginDAO->getTemplates();
                import('lib.pkp.classes.core.VirtualArrayIterator');
                $iterator = new VirtualArrayIterator($result, count($result), -1, -1);
                $pathPlugin = $this->getPluginPath() . '/';
                $templateMgr->addJavaScript($pathPlugin . 'js/jquery.min.js');
                $templateMgr->assign_by_ref('arrayTemplate', $iterator);
                $templateMgr->display($this->getTemplatePath() . 'vista/SelectExportIssueTemplate.tpl');
                break;
            //this case allows export issues
            case 'ExportIssueTemplate':
                $sessionManager = SessionManager::getManager();
                $session = $sessionManager->getUserSession();
                $issueId = $session->getSessionVar('Issues');
                $TemplateId = array_shift($args);
                $scielo = (bool) array_shift($args);
                if (!isset($issueId)) {
                    $issueId = array();
                }
                $templateMgr = & TemplateManager::getManager();
                $this->exportIssues($journal, $issueId, $TemplateId, $scielo, $request,$templateMgr);
                break;
            //this case allows delete template
            case 'deleteTemplate':
                $this->setBreadcrumbs(array(), true);
                $TemplateId = array(array_shift($args));
                $templateMgr = & TemplateManager::getManager();
                $result = $adaptablePluginDAO->deleteTemplate($TemplateId);
                if ($result) {
                    $url = str_replace('deleteTemplate/' . $TemplateId[0], '', $templateMgr->get_template_vars('currentUrl'));
                    $request->redirectUrl($url . "template");
                } else {
                    $templateMgr->display($this->getTemplatePath() . 'vista/wrongTemplate.tpl');
                }
                break;
            case 'putImage':
                $img = $request->getUserVar('imageData');
                $idArticle = $request->getUserVar('idArticle');
                $idIssue = $request->getUserVar('idIssue');
                $name = $request->getUserVar('name');
                $volumeId = $issueDao->getIssueById($idIssue)->getVolume();
                $basePath = $this->getPluginPath();
                $articles = $publishedArticleDao->getPublishedArticles($idIssue);
                $article = 0;
                for ($index = 0; $index < count($articles); $index++) {
                    if ($articles[$index]->getArticleId() === $idArticle) {
                        $article = $index + 1;
                    }
                }
                echo Converter::convertCanvasToImage($basePath, $img, $volumeId, $idIssue, $article, $name);
                break;
            //this is the initial page 
            default:
                $this->setBreadcrumbs();
                $templateMgr = & TemplateManager::getManager();
                $pathPlugin = $this->getPluginPath() . '/';
                $templateMgr->addJavaScript($pathPlugin . 'js/jquery.min.js');
                $templateMgr->display($this->getTemplatePath() . 'vista/index.tpl');
        }
    }

    /**
     * 
     */
    function exportIssue($request, $journal) {
        $issueId = $request->getUserVar('issueId');
        $articleId = $request->getUserVar('articleId');
        $templateId = $request->getUserVar('templateId');
        $html = $request->getUserVar('exportIssueMarkupHtml');
        $markupHtml = $request->getUserVar('exportIssueHtml');
        $scielo = filter_var($request->getUserVar('exportScielo'), FILTER_VALIDATE_BOOLEAN);
        $issueDao = & DAORegistry::getDAO('IssueDAO');
        $volumeId = $issueDao->getIssueById($issueId)->getVolume();
        $journal = & Request::getJournal();
        $journalTitle = $journal->getTitle("es_ES");

        $textMarkup = new TextMarkup();
        $textMarkup->setIdVolume($volumeId);
        $textMarkup->setIdIssue($issueId);
        $textMarkup->setIdArticle($articleId);
        $textMarkup->setIdTemplate($templateId);
        $textMarkup->setHtml($html);
        $textMarkup->setMarkupHtml($markupHtml);

        $adaptablePluginDAO = & DAORegistry::getDAO('AdaptablePluginDAO');
        $result = $adaptablePluginDAO->getNumberMarkup($volumeId, $issueId, $articleId, $templateId);
        if ($result) {
            $result = $adaptablePluginDAO->updateMarkup($textMarkup);
        } else {
            $result = $adaptablePluginDAO->saveMarkup($textMarkup);
        }

        $url = "issues";
        $zip = new ZipArchive();
        $tmp_file = $journalTitle . ".zip";
        if ($zip->open($tmp_file, ZipArchive::CREATE) === TRUE) {
            if ($scielo) {
                $zip->addEmptyDir($journalTitle);
                $zip->addEmptyDir($journalTitle . "/v" . $volumeId . "n" . $issueId);
                $zip->addEmptyDir($journalTitle . "/v" . $volumeId . "n" . $issueId . "/body");
                $zip->addEmptyDir($journalTitle . "/v" . $volumeId . "n" . $issueId . "/img");
                $zip->addEmptyDir($journalTitle . "/v" . $volumeId . "n" . $issueId . "/markup");
                $zip->addEmptyDir($journalTitle . "/v" . $volumeId . "n" . $issueId . "/pdf");
            }

            $publishedArticleDao = & DAORegistry::getDAO('PublishedArticleDAO');
            $articles = $publishedArticleDao->getPublishedArticles($issueId);

            for ($index = 0; $index < count($articles); $index++) {

                $result = $adaptablePluginDAO->getMarkup($volumeId, $issueId, $articles[$index]->getArticleId(), $templateId);

                $goodHTML = "<html><body>" . $result->getMarkupHtml() . "</body></html>";

                if ($scielo) {

                    foreach (glob($this->getPluginPath() . "/image/v" . $volumeId . "n" . $issueId . "a" . ($index + 1) . "[ft][1-9]*.*") as $filename) {
                        $fileinfo = pathinfo($filename);
                        $zip->addFile($filename, $journalTitle . "/v" . $volumeId . "n" . $issueId . "/img/" . $fileinfo["basename"]);
                    }

                    $zip->addFromString($journalTitle . "/v" . $volumeId . "n" . $issueId . "/body/v" . $volumeId . "n" . $issueId . "a" . ($index + 1) . ".html", $result->getHtml());
                    $zip->addFromString($journalTitle . "/v" . $volumeId . "n" . $issueId . "/markup/v" . $volumeId . "n" . $issueId . "a" . ($index + 1) . ".html", $goodHTML);

                    import('classes.file.ArticleFileManager');
                    $articleFileManager = new ArticleFileManager($articles[$index]->getArticleId());
                    $fileContent = & $articleFileManager->getFile($articles[$index]->getSubmissionFileId(), null, false);

                    if (pathinfo($fileContent->getFilePath())["extension"] === "pdf") {
                        $zip->addFile($fileContent->getFilePath(), $journalTitle . "/v" . $volumeId . "n" . $issueId . "/pdf/v" . $volumeId . "n" . $issueId . "a" . ($index + 1) . ".pdf");
                    } else if (pathinfo($fileContent->getFilePath())["extension"] === "docx") {
                        $nameFile = "doctopdf.pdf";
                        Parser::parserDocxToPdf($fileContent->getFilePath(), sys_get_temp_dir() . DIRECTORY_SEPARATOR, $nameFile);
                        $zip->addFile(sys_get_temp_dir() . DIRECTORY_SEPARATOR . $nameFile, $journalTitle . "/v" . $volumeId . "n" . $issueId . "/pdf/v" . $volumeId . "n" . $issueId . "a" . ($index + 1) . ".pdf");
                    }
                }

                $HTML = html_entity_decode(Remover::removeParagraphTags($goodHTML));
                $doc = new DOMDocument();
                $doc->encoding = 'UTF-8';
                $doc->loadXML($HTML);
                $doc->encoding = 'UTF-8';
                $HTML = $doc->saveXML();

                $zip->addFromString($journalTitle . "/v" . $volumeId . "n" . $issueId . "a" . ($index + 1) . ".xml", $HTML);
            }

            $zip->close();
        } else {
            echo 'Zip creation failed';
        }

        header('Content-disposition: attachment; filename=' . $tmp_file);
        header('Content-type: application/zip');
        readfile($tmp_file);
        unlink($tmp_file);
    }

    function exportIssues($journal, $issues, $TemplateId, $scielo, $request,$templateMgr) {
        $journalTitle = $journal->getTitle("es_ES");
        $issueDao = & DAORegistry::getDAO('IssueDAO');
        $adaptablePluginDAO = & DAORegistry::getDAO('AdaptablePluginDAO');
        $zip = new ZipArchive();
        $tmp_file = $journalTitle . ".zip";

        if ($zip->open($tmp_file, ZipArchive::CREATE) === TRUE) {
            for ($index1 = 0; $index1 < count($issues); $index1++) {
                $volumeId = $issueDao->getIssueById($issues[$index1])->getVolume();
                if ($scielo) {
                    $zip->addEmptyDir($journalTitle);
                    $zip->addEmptyDir($journalTitle . "/v" . $volumeId . "n" . $issues[$index1]);
                    $zip->addEmptyDir($journalTitle . "/v" . $volumeId . "n" . $issues[$index1] . "/body");
                    $zip->addEmptyDir($journalTitle . "/v" . $volumeId . "n" . $issues[$index1] . "/img");
                    $zip->addEmptyDir($journalTitle . "/v" . $volumeId . "n" . $issues[$index1] . "/markup");
                    $zip->addEmptyDir($journalTitle . "/v" . $volumeId . "n" . $issues[$index1] . "/pdf");
                }

                $publishedArticleDao = & DAORegistry::getDAO('PublishedArticleDAO');
                $articles = $publishedArticleDao->getPublishedArticles($issues[$index1]);

                for ($index = 0; $index < count($articles); $index++) {
                    $result = $adaptablePluginDAO->getMarkup($volumeId, $issues[$index1], $articles[$index]->getArticleId(), $TemplateId);
                    if ($result == null || $result->getHtml() === "" || $result->getMarkupHtml() === "") {
                        $templateMgr->display($this->getTemplatePath() . 'vista/errorMarkup.tpl');
                        break;
                    }
                    $goodHTML = "<html><body>" . $result->getMarkupHtml() . "</body></html>";
                    if ($scielo) {
                        foreach (glob($this->getPluginPath() . "/image/v" . $volumeId . "n" . $issues[$index1] . "a" . ($index + 1) . "[ft][1-9]*.*") as $filename) {
                            $fileinfo = pathinfo($filename);
                            $zip->addFile($filename, $journalTitle . "/v" . $volumeId . "n" . $issues[$index1] . "/img/" . $fileinfo["basename"]);
                        }
                        $zip->addFromString($journalTitle . "/v" . $volumeId . "n" . $issues[$index1] . "/body/v" . $volumeId . "n" . $issues[$index1] . "a" . ($index + 1) . ".html", $result->getHtml());
                        $zip->addFromString($journalTitle . "/v" . $volumeId . "n" . $issues[$index1] . "/markup/v" . $volumeId . "n" . $issues[$index1] . "a" . ($index + 1) . ".html", $goodHTML);

                        import('classes.file.ArticleFileManager');
                        $articleFileManager = new ArticleFileManager($articles[$index]->getArticleId());
                        $fileContent = & $articleFileManager->getFile($articles[$index]->getSubmissionFileId(), null, false);

                        if (pathinfo($fileContent->getFilePath())["extension"] === "pdf") {
                            $zip->addFile($fileContent->getFilePath(), $journalTitle . "/v" . $volumeId . "n" . $issues[$index1] . "/pdf/v" . $volumeId . "n" . $issues[$index1] . "a" . ($index + 1) . ".pdf");
                        } else if (pathinfo($fileContent->getFilePath())["extension"] === "docx") {
                            $nameFile = "doctopdf.pdf";
                            Parser::parserDocxToPdf($fileContent->getFilePath(), sys_get_temp_dir() . DIRECTORY_SEPARATOR, $nameFile);
                            $zip->addFile(sys_get_temp_dir() . DIRECTORY_SEPARATOR . $nameFile, $journalTitle . "/v" . $volumeId . "n" . $issues[$index1] . "/pdf/v" . $volumeId . "n" . $issues[$index1] . "a" . ($index + 1) . ".pdf");
                        }
                    }

                    $HTML = html_entity_decode(Remover::removeParagraphTags($goodHTML));
                    $doc = new DOMDocument();
                    $doc->encoding = 'UTF-8';
                    $doc->loadXML($HTML);
                    $doc->encoding = 'UTF-8';
                    $HTML = $doc->saveXML();

                    $zip->addFromString($journalTitle . "/v" . $volumeId . "n" . $issues[$index1] . "a" . ($index + 1) . ".xml", $HTML);
                }
            }

            $zip->close();
        } else {
            echo 'Zip creation failed';
            echo '<script type="text/javascript">';
            echo 'alert("No se pudo crear el zip");';
            echo '</script>';
        }


        if (!is_readable($tmp_file)) {
            echo '<script type="text/javascript">';
            echo 'alert("No se pudo descargar el archivo, verifique que '
            . 'los textos hayan sido marcados");';
            echo '</script>';
            $urlIssues = "../../exportIssues";
            $request->redirectUrl($urlIssues);
        }




        header('Content-disposition: attachment; filename=' . $tmp_file);
        header('Content-type: application/zip');
        readfile($tmp_file);
        unlink($tmp_file);
    }

    /**
     * Execute import/export tasks using the command-line interface.
     * @param $args Parameters to the plugin
     */
    function executeCLI($scriptName, &$args) {
        $this->usage($scriptName);
    }

    /**
     * Display the command-line usage information
     */
    function usage($scriptName) {
        echo "USAGE NOT AVAILABLE.\n"
        . "This plugin must not be used in the command-line.\n";
    }

}
