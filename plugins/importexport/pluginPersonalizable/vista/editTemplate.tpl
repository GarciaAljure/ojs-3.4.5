{**
* plugins/importexport/sample/issues.tpl
*
* Copyright (c) 2013-2014 Simon Fraser University Library
* Copyright (c) 2003-2014 John Willinsky
* Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
*
* List of issues to potentially export
*
*}


{strip}
    {assign var="pageTitle" value="plugins.importexport.PluginPersonalizable.editTemplate"}
    {include file="common/header.tpl"}
{/strip}
<form action="{plugin_url path="saveEditTemplate"}" method="post" id="formEditTree" >
    <p>{translate key="plugins.importexport.PluginPersonalizable.editTemplateDescription"}</p>
    <label>{translate key="plugins.importexport.PluginPersonalizable.name"}</label>
    <input name="name" id="name" type="text" class="textField" value="{$template->getName()}" />
    <input name="id_template" id="id_template" type="hidden"  value="{$template->getId()}" />
    <input name="text_html" id="text_html" type="hidden"  />
    <input name="plugin_url" id="plugin_url" type="hidden" value="{plugin_url}"  />
    <input type="button" value="{translate key="common.save"}" class="button defaultButton" onclick="copyHtmltoInput()"/>
</form>
<br>
<div class="easy-tree" id="xmlTree">
    <ul id="treeHtml">
        {$template->getText()}
    </ul>
</div>

{include file="common/footer.tpl"}

<script >
    {literal}
        var elementHtml = null;

        //var nuevo = jQuery.noConflict();
        $(window).load(function () {
            $('.easy-tree').EasyTree({
                addable: true,
                editable: true,
                deletable: true
            });



        });

        function copyHtmltoInput() {
            if ($("#name").val() === "") {
                alert("Debe introducir un nombre para la plantilla");
                return;
            }

            $("#text_html").val($("#treeHtml").html());
            $("#formEditTree").submit();
        }

    {/literal}
</script>

