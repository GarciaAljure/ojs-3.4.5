{**
 * plugins/importexport/native/importError.tpl
 *
 * Copyright (c) 2013-2014 Simon Fraser University Library
 * Copyright (c) 2003-2014 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Display an error message for an aborted import process.
 *
 *}
{strip}
{assign var="pageTitle" value="plugins.importexport.PluginPersonalizable.markup.error"}
{include file="common/header.tpl"}
{/strip}
<div id="importError">
<p>{translate key="plugins.importexport.PluginPersonalizable.markup.error.description"}</p>

</div>
{include file="common/footer.tpl"}
