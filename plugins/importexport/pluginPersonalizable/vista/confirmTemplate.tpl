{**
* plugins/importexport/sample/issues.tpl
*
* Copyright (c) 2013-2014 Simon Fraser University Library
* Copyright (c) 2003-2014 John Willinsky
* Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
*
* List of issues to potentially export
*
*}
{strip}
    {assign var="pageTitle" value="plugins.importexport.PluginPersonalizable.viewTemplate"}
    {include file="common/header.tpl"}
{/strip}

{assign var=contentXml value=$content}

<br/>

<form action="{plugin_url path="importTemplate"}" method="post" enctype="multipart/form-data" id="formXml">

    <p>{translate key="plugins.importexport.PluginPersonalizable.import.confirm"}</p>
    <label>{translate key="plugins.importexport.PluginPersonalizable.name"}</label>
    <input name="name" id="name" type="text" class="textField" value="" />

    <input name="plugin_url" id="plugin_url" type="hidden" class="textField" value="{plugin_url}" />
    <input name="codeXML" id="codeXML" type="hidden" value="{$content|escape}" />
    <input name="import" onclick="copyHtmltoInput()" type="button" class="button defaultButton" value="{translate key="common.confirm"}" />
</form>
<br/>
<br/>
<!--<div class="codeEditor" style="border: 1px solid #76ae76;">
     <pre class="brush: xml;" id="XmlTextarea">
{*$content*}
</pre>
</div> -->


<div class="easy-tree" id="xmlTree">
    <ul id="treeHtml">


    </ul>
</div>









{include file="common/footer.tpl"}

<script >
    {literal}
        $("#XmlTextarea").hide();
        //SyntaxHighlighter.all();

        function copyHtmltoInput() {
            if ($("#name").val()==="") {
                alert("Debe introducir un nombre para la plantilla");
                return;
            }

            $("#codeXML").val($("#treeHtml").html());
            $("#formXml").submit();
        }

        $(window).load(function () {
            var html = "";
            $("#treeHtml").append(traverse($.parseXML($("#codeXML").val()), html));
            $('.easy-tree').EasyTree({
                addable: true,
                editable: true,
                deletable: true
            });

            //console.log(traverse($.parseXML($("#codeXML").val()), html));

        });


        /*function traverse(tree, html) {
         if (tree.hasChildNodes()) {
         html = '<ul><li>';
         html = tree.tagName;
         console.log(tree.children());
         for (var i = 0; i < tree.children.length; i++)
         {
         return traverse(tree.children(i));
         }
         html = '</li></ul>';
         }
         return html;
         }*/



        function traverse(tree, html) {

            $(tree).contents().each(function () {
                if (this.nodeType !== 3) {
                    if (html === "") {
                        html += "<li id='startUl'>" + this.nodeName;
                    } else {
                        html += "<li>" + this.nodeName;
                    }
                    if (this.childElementCount > 0) {
                        html += '<ul>';

                        html = traverse(this, html);
                        html += '</ul>';
                    }
                    html += "</li>";

                }
            });

            return html;





            /*   $(tree).contents().each(function () {
             if (this.nodeType !== 3) {
             console.log(tree.hasChildNodes());
             
             html += '<li>' + this.nodeName + '</li>';
             traverse(this, html);
             
             }
             });
             
             
             return html;*/
        }

    {/literal}
</script>

