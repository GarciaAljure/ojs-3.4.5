{**
* plugins/importexport/sample/issues.tpl
*
* Copyright (c) 2013-2014 Simon Fraser University Library
* Copyright (c) 2003-2014 John Willinsky
* Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
*
* List of issues to potentially export
*
*}
{strip}
    {assign var="pageTitle" value="plugins.importexport.PluginPersonalizable.titleTemplate"}
    {assign var="pageCrumbTitle" value="plugins.importexport.PluginPersonalizable.titleTemplate"}
    {include file="common/header.tpl"}
{/strip}

<br/>

<a href="#" id="createTemplate" class="action">{translate key="plugins.importexport.PluginPersonalizable.createTemplate"}</a>
<a href="#" id="importTemplate" class="action">{translate key="plugins.importexport.PluginPersonalizable.import"}</a>

<div id="xmlImport">

    <h3>{translate key="plugins.importexport.PluginPersonalizable.import"}</h3>
    <p>{translate key="plugins.importexport.PluginPersonalizable.import.description"}</p>

    <form action="{plugin_url path="import"}" method="post" enctype="multipart/form-data">
        <input type="file" class="uploadField" name="importFile" id="import" /> <input name="import" type="submit" class="button defaultButton" value="{translate key="common.import"}" />
        <input type="hidden" name="temporaryFileId" id="temporaryFileId" value="" />

    </form>
</div>

<br>





<div  id="xmlTree">
    <h3>{translate key="plugins.importexport.PluginPersonalizable.createTree"}</h3>
    <p>{translate key="plugins.importexport.PluginPersonalizable.createTree.description"}</p>

    <form action="{plugin_url path="CreateTreeTemplate"}" method="post" enctype="multipart/form-data" id="formTree">
        <label>{translate key="plugins.importexport.PluginPersonalizable.name"}</label>
        <input name="name" id="name" type="text" class="textField" value="" />
        <input name="html" id="html" type="hidden" class="textField" value="" />
        <input name="plugin_url" id="plugin_url" type="hidden" class="textField" value="{plugin_url}" />
        <input name="import"  onclick="copyHTMLtoInput()" type="button" class="button defaultButton"  value="{translate key="common.confirm"}" />
    </form>

    <br>
    <br>

    <div class="easy-tree" >

        <ul id="treeHtml">
            <li id="startUl">Inicio</li>

        </ul>
    </div>
</div>



{include file="common/footer.tpl"}

<script >
    {literal}


        $(document).ready(function () {

            $("#xmlTree").hide();
            $("#xmlImport").hide();


            $("#createTemplate").click(function () {
                $("#xmlImport").hide();
                $("#xmlTree").show();

            });
            $("#importTemplate").click(function () {
                $("#xmlTree").hide();
                $("#xmlImport").show();
            });


        });


        $(window).load(function () {
            $('.easy-tree').EasyTree({
                addable: true,
                editable: true,
                deletable: true
            });

        });

        function copyHTMLtoInput() {
            $("#html").val($("#treeHtml").html());
            $("#formTree").submit();
        }



    {/literal}
</script>

