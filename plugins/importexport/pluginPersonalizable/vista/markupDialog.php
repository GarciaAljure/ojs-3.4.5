<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="../lib/Bootstrap/bootstrap.css" >
        <link rel="stylesheet" href="../lib/EasyTree/css/easyTree.css" >
        <link rel="stylesheet" href="../css/styleMarkup.css" >

        <script src="../js/jquery.min.js"></script>
        <script src="../lib/Bootstrap/bootstrap.min.js"></script>
        <script src="../lib/ckeditor/ckeditor.js"></script>
        <script src="../lib/ckeditor/adapters/jquery.js"></script>
        <script src="../lib/EasyTree/src/easyTree.js"></script>

        <title>Ventana de marcado de texto</title>
    </head>
    <body>
        <p><strong>Editor de texto</strong></p>
        <textarea name="editor2" id="editor2" style="width: 100%; margin-top: 10px;"></textarea>
        <p><strong>Árbol de etiquetas</strong></p>
        <div class="easy-tree" id="xmlTreePopup" style="width: 100%; height: 200px; font-size: 12px;  font-family: Verdana,Arial,Helvetica,sans;">
            <ul id="treeHtmlPopup">

            </ul>
        </div>
        <input  class="btn btn-default pull-right"  type="button" value="Aceptar" onclick="markup()">

        <div id="menuPopup" class="divMenuPopup">
            <ul class="ulMenu">
                <li id="asign" class="liMenuPopup">Asignar etiqueta</li>
            </ul>
        </div>

    </body>
    <script>
        var elementHtmlPopup = null;
        CKEDITOR.replace('editor2', {
            extraPlugins: 'resize,image,table',
            removePlugins: 'elementspath'
        });


        //var nuevo = jQuery.noConflict();
        $(window).load(function () {
            $('.easy-tree').EasyTree({
                addable: false,
                editable: false,
                deletable: false
            });
            $("#treeHtmlPopup").find("a").each(function (index, element) {
                //console.log('El elemento con el índice ' + index + ' contiene ' + $(element).text());
                $(element).bind("contextmenu", function (e) {
                    elementHtmlPopup = element;
                    $("#menuPopup").css({'display': 'block', 'left': e.pageX, 'top': e.pageY});
                    return false;
                });
            });

        });

        $(window).ready(function () {
            $("#menuPopup").hide();
            $("#treeHtmlPopup").append("<li>" + $(window.opener.list_html).html() + "</li>");
            var word = window.opener.selectedWord;
            word = String(word).replace(/,/g, "<br>");
            CKEDITOR.instances.editor2.setData(word);

            $(document).bind("contextmenu", function (e) {

                return false;
            });
            //cuando hagamos click, el menú desaparecerá
            $(document).click(function (e) {
                if (e.button == 0) {
                    $("#menuPopup").css("display", "none");
                }
            });
            //si pulsamos escape, el menú desaparecerá
            $(document).keydown(function (e) {
                if (e.keyCode == 27) {
                    $("#menuPopup").css("display", "none");
                }
            });
            //controlamos los botones del menú
            $("#menuPopup").click(function (e) {

                // El switch utiliza los IDs de los <li> del menú
                switch (e.target.id) {
                    case "asign":
                        var editorC = CKEDITOR.instances.editor2;
                        if (editorC.getSelectedHtml() === null || editorC.getSelectedHtml().getHtml().toString() === "") {
                            alert("Debe seleccionar el texto para realizar esta función");
                            break;
                        }
                        var selectedContent = editorC.getSelectedHtml().getHtml();

                        editorC.insertHtml(escapeHtml("<" + $.trim($(elementHtmlPopup).text()) + ">") + selectedContent + escapeHtml("</" + $.trim($(elementHtmlPopup).text()) + ">"));
                        elementHtmlPopup = null;
                        break;
                }

            });
        });

        function escapeHtml(string) {
            var entityMap = {
                "&": "&amp;",
                "<": "&lt;",
                ">": "&gt;",
                '"': '&quot;',
                "'": '&#39;',
                //"/": '&#x2F;',
                "á": '&aacute;',
                "é": '&eacute;',
                "í": '&iacute;',
                "ó": '&oacute;',
                "ú": '&uacute;',
                "ñ": '&ntilde;',
                "Á": '&Aacute;',
                "É": '&Eacute;',
                "Í": '&Iacute;',
                "O": '&Oacute;',
                "Ú": '&Uacute;',
                "Ñ": '&Ntilde;'
            };
            return String(string).replace(/[áéíóúÁÉÍÓÚ&<>"'ñÑ]/g, function (s) {
                return entityMap[s];
            });
        }

        function markup() {
            var word = CKEDITOR.instances.editor2.getData();

            console.log(word);

            word = String(word).replace(/<p>/gi, "");
            word = String(word).replace(/<\/p>/gi, "");
            word = String(word).replace(/<br \/>/gi, "");
            elementHtmlPopup = null;
            window.opener.elementHtml = null;
            window.opener.selectedWord = null;
            window.opener.list_html = null;
            console.log(word);
            window.opener.CKEDITOR.instances.editor1.insertHtml(word);
            window.close();
        }

    </script>
</html>

