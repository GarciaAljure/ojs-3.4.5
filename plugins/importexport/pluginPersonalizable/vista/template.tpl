{**
* plugins/importexport/sample/issues.tpl
*
* Copyright (c) 2013-2014 Simon Fraser University Library
* Copyright (c) 2003-2014 John Willinsky
* Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
*
* List of issues to potentially export
*
*}
{strip}
    {assign var="pageTitle" value="plugins.importexport.PluginPersonalizable.selectTemplate"}

    {include file="common/header.tpl"}
{/strip}




<form action="{plugin_url path="createTemplate"}" method="post"  >
    <p>{translate key="plugins.importexport.PluginPersonalizable.createTemplateDescription"}</p>
    <input type="submit" value="{translate key="plugins.importexport.PluginPersonalizable.createTemplate"}" class="button defaultButton "/>
    <br>
    <br>
    <div id="articles">

        <table width="100%" class="listing">
            <tr>
                <td colspan="4" class="headseparator">&nbsp;</td>
            </tr>
            <tr class="heading" valign="bottom">

                <td width="40%">{translate key="plugins.importexport.PluginPersonalizable.nameTemplate"}</td>
                <td width="40%">{translate key="plugins.importexport.PluginPersonalizable.dateTemplate"}</td>
                <td width="10%" align="left" >{translate key="plugins.importexport.PluginPersonalizable.action"}</td>
                <td width="10%" ></td>

            </tr>
            <tr>
                <td colspan="4" class="headseparator">&nbsp;</td>
            </tr>

            {iterate from=arrayTemplate item=template}
            <tr valign="top">
                <td>{$template->getName()}</td>
                <td>{$template->getDate()}</td>
                <td align="left"><a href="{plugin_url path="editTemplate"|to_array:$template->getId()}" class="action">{translate key="common.edit"}</a></td>
                <td align="left"><a href="{plugin_url path="deleteTemplate"|to_array:$template->getId()}"  onclick="return confirm('{translate|escape:"jsparam" key="plugins.importexport.PluginPersonalizable.confirmDeleteTemplate"}')"  class="action">{translate key="common.delete"}</a></td>

            </tr>

            <tr>
                <td colspan="4" class="separator">&nbsp;</td>
            </tr>
            {/iterate}

            {if empty($template)}
                <tr>
                    <td colspan="4" class="nodata">{translate key="common.none"}</td>
                </tr>
                <tr>
                    <td colspan="4" class="endseparator">&nbsp;</td>
                </tr>

            {/if}


        </table>

    </div>

</form>
{include file="common/footer.tpl"}
