{**
* plugins/importexport/sample/issues.tpl
*
* Copyright (c) 2013-2014 Simon Fraser University Library
* Copyright (c) 2003-2014 John Willinsky
* Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
*
* List of issues to potentially export
*
*}
{strip}
    {assign var="pageTitle" value="plugins.importexport.PluginPersonalizable.selectTemplate"}

    {include file="common/header.tpl"}
{/strip}


<div id="articles">

    <p>{translate key="plugins.importexport.PluginPersonalizable.selectTemplateDescription"}</p>

    <br>
    <table width="100%" class="listing">
        <tr>
            <td colspan="3" class="headseparator">&nbsp;</td>
        </tr>
        <tr class="heading" valign="bottom">

            <td width="40%">{translate key="plugins.importexport.PluginPersonalizable.nameTemplate"}</td>
            <td width="40%">{translate key="plugins.importexport.PluginPersonalizable.dateTemplate"}</td>
            <td width="20%" >{translate key="plugins.importexport.PluginPersonalizable.action"}</td>

        </tr>
        <tr>
            <td colspan="3" class="headseparator">&nbsp;</td>
        </tr>

        {iterate from=arrayTemplate item=template}
        <tr valign="top">
            <td>{$template->getName()}</td>
            <td>{$template->getDate()}</td>
            <td ><a href="{plugin_url path="SelectedTemplate"|to_array:$issueId:$template->getId()}" class="action">{translate key="common.select"}</a></td>


        </tr>

        <tr>
            <td colspan="3" class="separator">&nbsp;</td>
        </tr>
        {/iterate}

        {if empty($template)}
            <tr>
                <td colspan="3" class="nodata">{translate key="common.none"}</td>
            </tr>
            <tr>
                <td colspan="3" class="endseparator">&nbsp;</td>
            </tr>

        {/if}


    </table>

</div>


{include file="common/footer.tpl"}
