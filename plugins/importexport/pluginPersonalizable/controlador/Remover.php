<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Remover
 *
 * @author CarlosAndres
 */
class Remover {

    /**
     * this function allows remove empty tag 
     * @param  $str
     * @param  $repto
     * @return String
     */
    public static function removeEmptyTagsRecursive($str, $repto = NULL) {
        //** Return if string not given or empty.
        if (!is_string($str) || trim($str) == '')
            return $str;

        //** Recursive empty HTML tags.
        return preg_replace(
                //** Pattern written by Junaid Atari.
                '/<([^<\/>]*)>([\s]*?|(?R))<\/\1>/imsU',
                //** Replace with nothing if string empty.
                !is_string($repto) ? '' : $repto,
                //** Source string
                $str
        );
    }

    /**
     * this function allows remove repetitive tag
     * @param  $str
     * @return String
     */
    public static function removeRepetitiveTags($str) {
        return str_replace("</u><u>", "", str_replace("</em><em>", "", str_replace("</strong><strong>", "", $str)));
    }

    /**
     * this function allows remove tag <p>
     * @param type $str
     * @return type
     */
    public static function removeParagraphTags($str) {
        return str_replace("<p>", "", str_replace("</p>", "", $str));
    }

}
