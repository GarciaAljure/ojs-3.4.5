<?php

set_time_limit(0);
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Parser
 *
 * @author CarlosAndres
 */
class Parser {

    /**
     * this function allows convert a file docx to html
     * 
     * @param type $filePath
     * @param type $basePath
     * @param type $pluginPath
     * @param type $volumeId
     * @param type $issue
     * @param type $article
     * @return string
     */
    public static function parserDocxToHtml($filePath, $basePath, $pluginPath, $volumeId, $issue, $article) {
        // Create new ZIP archive
        $relsFile = "";
        $relations = array();
        $images = array();
        $zip = new ZipArchive;
        $dataFile = 'word/document.xml';
// Open received archive file
        if (true === $zip->open($filePath)) {
            // If done, search for the data file in the archive
            if (($index = $zip->locateName($dataFile)) !== false) {
                // If found, read it to the string
                $data = $zip->getFromIndex($index);

                // Load XML from a string
                // Skip errors and warnings
                // set location of docx text content file
                $relsFile = $zip->getFromName("word/_rels/document.xml.rels");
                $relsReader = new XMLReader;
                $relsReader->XML($relsFile);
                while ($relsReader->read()) {
                    if ($relsReader->nodeType == XMLREADER::ELEMENT && $relsReader->name === 'Relationship') {
                        $node = trim($relsReader->readOuterXml());
                        $relations[$relsReader->getAttribute('Id')] = array(
                            'id' => $relsReader->getAttribute('Id'),
                            'type' => $relsReader->getAttribute('Type'),
                            'target' => $relsReader->getAttribute('Target')
                        );
                    }
                }


                $reader = new XMLReader;
                $reader->XML($data);

                // set up variables for formatting
                $text = '';
                $figure = 0;

                $htmlBlock = '';
                $tableCount = 0;
                $tocRef = null;
                $tocRefWrCount = 0;
                $toc = array();

                $tableCols = array();
                $tableRowSpan = array();
                $tableX = 0;
                $tableY = 0;
                $inTable = false;
                $inTableCell = false;

                $formatting['bold'] = 'closed';
                $formatting['italic'] = 'closed';
                $formatting['underline'] = 'closed';
                $formatting['header'] = 0;

// loop through docx xml dom
                while ($reader->read()) {
                    // look for new paragraphs
                    if ($reader->nodeType == XMLREADER::END_ELEMENT) {
                        // echo "<pre>*Node name: &lt;/" . $reader->name . "&gt;</pre>\n";
                        switch ($reader->name) {
                            case 'w:tbl':
                                $inTable = false;
                                $text .= "</table>\n";
                                foreach ($tableRowSpan as $col => $count) {
                                    $text = str_replace('rowspan="[' . $col . ']', 'rowspan="' . $count, $text);
                                }
                                break;
                            case 'w:tr':
                                $text .= "</tr>\n";
                                break;
                            case 'w:tc':
                                $inTableCell = false;
                                if (array_key_exists('r' . $tableX, $tableRowSpan) &&
                                        $tableRowSpan['r' . $tableX] > 1) {
                                    unset($htmlBlock);
                                    break;
                                }
                                if (isset($htmlBlock)) {
                                    $text .= $htmlBlock;
                                    unset($htmlBlock);
                                }

                                $text .= "</td>\n";
                                break;
                            case 'w:tblGrid':
                                $text .= "</colgroup>\n";
                                $totalWidth = 0;
                                foreach ($tableCols as $w) {
                                    $totalWidth += $w;
                                }
                                $widthPct = $totalWidth / 100;
                                $tableX = 0;
                                foreach ($tableCols as $w) {
                                    $tableX++;
                                    $pct = (int) ($w / $widthPct);
                                    $text = str_replace('width="[col' . $tableX . ']', 'width="' . $pct, $text);
                                    $totalWidth += $w;
                                }
                                break;
                        }
                    }
                    if ($reader->nodeType != XMLREADER::ELEMENT) {
                        continue;
                    }
                    switch ($reader->name) {
                        case 'w:tbl':
                            $tableCount++;
                            $text .= "<table border='1'>\n";
                            $tableCols = array();
                            $tableRowSpan = array();
                            $tableX = 0;
                            $tableY = 0;
                            $inTable = true;
                            $inTableCell = false;
                            break;
                        case 'w:tblGrid':
                            $tableX = 0;
                            $text .= "<colgroup>\n";
                            break;
                        case 'w:gridCol':
                            $tableX++;
                            $text .= "<col width=\"[col$tableX]%\" />\n";
                            $tableCols[] = $reader->getAttribute("w:w");
                            break;
                        case 'w:tr':
                            $tableX = 0;
                            $tableY++;
                            $text .= "<tr>\n";
                            break;
                        case 'w:tc':
                            $inTableCell = true;
                            $tableX++;
                            $td = "<td";
                            $addTd = true;
                            $p = $reader->readInnerXML();
                            if (strstr($p, 'w:gridSpan ')) {
                                preg_match('/w:gridSpan.+?w:val="([^"]+)"/', $p, $matches);
                                if (sizeof($matches) == 2) {
                                    $td .= ' colspan="' . $matches[1] . '"';
                                }
                            }

                            if (strstr($p, 'w:vMerge')) {
                                preg_match('/w:vMerge.+?w:val="([^"]+)"/', $p, $matches);
                                if (sizeof($matches) == 2 && $matches[1] == "restart") {
                                    if (array_key_exists('r' . $tableX, $tableRowSpan)) {
                                        $text = str_replace('rowspan="[r' . $tableX . ']', 'rowspan="' . $tableRowSpan['r' . $tableX], $text);
                                        unset($tableRowSpan['r' . $tableX]);
                                    }
                                    $td .= ' rowspan="[r' . $tableX . ']"';
                                    $tableRowSpan['r' . $tableX] = 1;
                                } else {
                                    $addTd = false;
                                    $tableRowSpan['r' . $tableX] ++;
                                }
                            } else {
                                if (array_key_exists('r' . $tableX, $tableRowSpan)) {
                                    $text = str_replace('rowspan="[r' . $tableX . ']', 'rowspan="' . $tableRowSpan['r' . $tableX], $text);
                                    unset($tableRowSpan['r' . $tableX]);
                                }
                            }
                            if ($addTd) {
                                $text .= $td . ">\n";
                            }
                            break;
                    }


                    if ($reader->name === 'w:p') {
                        $elementId = null;
                        $title = "";

                        // set up new instance of XMLReader for parsing paragraph independantly
                        $paragraph = new XMLReader;
                        $p = $reader->readOuterXML();
                        $paragraph->xml($p);


                        // search for heading
                        preg_match('/<w:pStyle w:val="(Heading.*?[1-6])"/', $p, $matches);
                        if (sizeof($matches) >= 2) {
                            switch ($matches[1]) {
                                case 'Heading1':
                                    $formatting['header'] = 1;
                                    break;
                                case 'Heading2':
                                    $formatting['header'] = 2;
                                    break;
                                case 'Heading3':
                                    $formatting['header'] = 3;
                                    break;
                                case 'Heading4':
                                    $formatting['header'] = 4;
                                    break;
                                case 'Heading5':
                                    $formatting['header'] = 5;
                                    break;
                                case 'Heading6':
                                    $formatting['header'] = 6;
                                    break;
                                default:
                                    $formatting['header'] = 0;
                                    break;
                            }
                        } else {
                            $formatting['header'] = 0;
                        }

                        // open h-tag or paragraph
                        if (!($inTable && !$inTableCell)) {
                            $text .= ($formatting['header'] > 0) ? '<h' . $formatting['header'] . '>' : '<p>';
                        }

                        // loop through paragraph dom
                        while ($paragraph->read()) {

                            // look for elements
                            if ($paragraph->nodeType == XMLREADER::END_ELEMENT) {
                                //		echo "<pre>Node name: &lt;/" . $paragraph->name . "&gt;</pre>\n";
                            }
                            if ($paragraph->nodeType != XMLREADER::ELEMENT) {
                                continue;
                            }
                            $paragraphName = $paragraph->name;
                            //echo "<pre>Node name: &lt;" . $paragraph->name . "&gt;</pre>\n";

                            if ($paragraphName === 'w:hyperlink') {
                                $node = trim($paragraph->readOuterXml());

                                preg_match('/.*w:hyperlink.+w:anchor="([^"]+)"/', $node, $matches);
                                if (sizeof($matches) == 2) {

                                    $tocRefWrCount = substr_count($node, '<w:r ');

                                    $tocRef = $matches[1];
                                    $text .= '<a href="#' . $tocRef . '">';
                                    $toc[$tocRef] = $tocRef;
                                }
                            } else if ($paragraphName === 'pic:pic') {
                                $figure++;
                                $picName = null;
                                $picId = null;




                                $picNode = new XMLReader;
                                $p = $paragraph->readOuterXML();
                                $picNode->xml($p);

                                while ($picNode->read()) {
                                    if ($picNode->nodeType != XMLREADER::ELEMENT) {
                                        continue;
                                    }
                                    $nodeName = $picNode->name;

                                    if ($nodeName === 'pic:cNvPr') {

                                        $picName = $picNode->getAttribute("name");
                                    } else if ($nodeName === 'a:blip') {
                                        $picId = $picNode->getAttribute("r:embed");
                                    }
                                }

                                if (isset($picId) && array_key_exists($picId, $relations)) {
                                    $relation = $relations[$picId];
                                    $separator = "/";

                                    //var_dump($zip->statName("word/" . $relation["target"]));
                                    //exit();


                                    if (($position = $zip->locateName("word/" . $relation["target"])) !== false) {

                                        $filenameIMG = $zip->getNameIndex($position);

                                        $fileinfo = pathinfo($filenameIMG);
                                        //mkdir($pluginPath . DIRECTORY_SEPARATOR . "image");
                                        copy("zip://" . $filePath . "#" . $filenameIMG, $pluginPath . $separator . "image" . $separator . "v" . $volumeId . "n" . $issue . "a" . $article . "f" . $figure . "." . $fileinfo['extension']);
                                    }



                                    $pathname = $basePath . $separator . "image" . $separator . "v" . $volumeId . "n" . $issue . "a" . $article . "f" . $figure . "." . $fileinfo['extension'];

                                    $arrayAtributes = getimagesizefromstring($zip->getFromName("word/" . $relation["target"]));

                                    $text .= '<img src="' . $pathname . '"  width="' . $arrayAtributes[0] . '"  height="' . $arrayAtributes[1] . '"/>';
                                }
                            } else if ($paragraphName === 'w:r') {
                                $tocRefWrCount--;
                                $node = trim($paragraph->readInnerXML());

//                                if (strstr($node, '<w:br ')) {
                                //                                    $text .= '<br>';
//                                }
                                $isBold = strstr($node, '<w:b/>');
                                $isItalic = strstr($node, '<w:i/>');
                                $isUnderline = strstr($node, '<w:u ');


                                $formatting['bold'] = (strstr($node, '<w:b/>')) ? (($formatting['bold'] == 'closed') ? 'open' : $formatting['bold']) : (($formatting['bold'] == 'opened') ? 'close' : $formatting['bold']);
                                $formatting['italic'] = (strstr($node, '<w:i/>')) ? (($formatting['italic'] == 'closed') ? 'open' : $formatting['italic']) : (($formatting['italic'] == 'opened') ? 'close' : $formatting['italic']);
                                $formatting['underline'] = (strstr($node, '<w:u ')) ? (($formatting['underline'] == 'closed') ? 'open' : $formatting['underline']) : (($formatting['underline'] == 'opened') ? 'close' : $formatting['underline']);

                                // build text string of doc
                                $text .= ($isBold ? '<strong>' : '') .
                                        ($isItalic ? '<em>' : '') .
                                        ($isUnderline ? '<u>' : '') .
                                        htmlentities($paragraph->expand()->textContent) .
                                        ($isUnderline ? '</u>' : '') .
                                        ($isItalic ? '</em>' : '') .
                                        ($isBold ? '</strong>' : '');


                                // reset formatting variables
                                foreach ($formatting as $key => $format) {
                                    if ($format == 'open') {
                                        $formatting[$key] = 'opened';
                                    }
                                    if ($format == 'close') {
                                        $formatting[$key] = 'closed';
                                    }
                                }

                                if (isset($tocRef) && $tocRefWrCount <= 0) {
                                    $text .= '</a>';
                                    unset($tocRef);
                                }
                            }
                        }
                        if (isset($tocRef)) {
                            $text .= '</a>';
                            unset($tocRef);
                        }
                        $text .= ($formatting['header'] > 0) ? '</h' . $formatting ['header'] . '>' : '</p>';
                    }
                }





                $reader->close();

                $zip->close();

                $doc = new DOMDocument();
                $doc->encoding = 'UTF-8';
                $doc->loadHTML($text);
                $goodHTML = simplexml_import_dom($doc)->asXML();
                return $goodHTML;
            }
        }
        // In case of failure return empty string
        return "";
    }

    public static function parserPdfToDocxl($filePath, $Path, $nameFile) {
        $myPdf2Doc = new Pdf2Doc();
        $myPdf2Doc->apiKey = Parser::getApiKey();
        $myPdf2Doc->inputPdfLocation = $filePath;
        //please make sure that the dir is writable chmod 777
        $myPdf2Doc->outputDocLocation = $Path . $nameFile . "docx";
        //$result will be OK or APINOK or Error Message 
        $result = $myPdf2Doc->Pdf2Doc();
        return $result;
    }

    public static function parserDocxToPdf($filePath, $Path, $nameFile) {



        $myDoc2Pdf = new Doc2PdfConverter();
        $myDoc2Pdf->apiKey = Parser::getApiKey();
        //$myDoc2Pdf->apiKey = "oskabvsorj5133";
        $myDoc2Pdf->inputDocLocation = $filePath;
        $myDoc2Pdf->outputPdfLocation = $Path . $nameFile;
        $result = $myDoc2Pdf->Doc2PdfConvert();
        return $result;
    }

    public static function getApiKey() {

        $doc = new DOMDocument();
        $doc->load(str_replace("controlador", "", __DIR__ . 'apiKey.xml'));
        $count = $doc->getElementsByTagName("count");
        $key = $doc->getElementsByTagName("key");
        $apiKey = $key->item(0)->nodeValue;

        $count->item(0)->nodeValue = ((int) ($count->item(0)->nodeValue)) - 1;
        if ((int) ($count->item(0)->nodeValue) === 0) {
            $key->item(0)->parentNode->removeChild($key->item(0));
            $count->item(0)->nodeValue = 20;
        }
        $doc->saveXML();
        $doc->save(str_replace("controlador", "", __DIR__ . 'apiKey.xml'));
        return $apiKey;
    }

}
