<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Parser
 *
 * @author CarlosAndres
 */


class Converter {

    /**
     * this function allows convert a html table (canvas) to gif
     * @param type $basePath
     * @param type $img
     * @param type $volumeId
     * @param type $IdIssue
     * @param type $IdArticle
     * @param type $name
     * @return type
     */
    public static function convertCanvasToImage($basePath, $img, $volumeId, $IdIssue, $IdArticle, $name) {

        //$img = str_replace('data:image/png;base64,', '', $img);
        //$img = str_replace(' ', '+', $img);
        //$fileData = base64_decode($img);
        $img = imagecreatefrompng($img);


        //saving
        $fileName = $basePath . DIRECTORY_SEPARATOR . 'image' . DIRECTORY_SEPARATOR . "v" . $volumeId . "n" . $IdIssue . "a" . $IdArticle . "t" . ($name + 1) . '.gif';
        return imagegif($img, $fileName);
        //return file_put_contents($fileName, $fileData);
    }

    
}
