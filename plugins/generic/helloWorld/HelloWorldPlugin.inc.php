<?php

import('plugins.GenericPlugin');

class HelloWorldPlugin extends GenericPlugin {
	function getName() {
		return 'HelloWorldPlugin';
	}

	function getDisplayName() {
		return 'Hello World Plugin';
	}

	function getDescription() {
		return 'This is my plugin description.';
	}
}

?>
